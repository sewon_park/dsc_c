#include "Flatness.h"

void Flatness::Init(dsc_cfg_t *dsc_cfg)
{
	InitQuantTable(dsc_cfg);
	prevFlatnessType = 0;
}

void Flatness::DecFlatnessDecision(int groupCount, int firstFlat_dec)
{
	// *MODEL NOTE* MN_ENC_FLATNESS_DECISION
	if ((groupCount % GROUPS_PER_SUPERGROUP) == firstFlat_dec)
		origIsFlat = 1;
	else
		origIsFlat = 0;
}

int Flatness::QpAdjustment(dsc_cfg_t *dsc_cfg, int master_qp, int flatnessType, int * st_qp)
{
	// *MODEL NOTE* MN_FLAT_QP_ADJ
	if (origIsFlat && (master_qp < dsc_cfg->rc_range_parameters[NUM_BUF_RANGES - 1].range_max_qp))
	{
		if ((flatnessType == 0) || (master_qp<SOMEWHAT_FLAT_QP_THRESH)) // Somewhat flat
		{
			*st_qp = MAX(*st_qp - 4, 0);
			return *st_qp;
		}
		else {		// very flat
			*st_qp = 1 + (2 * (dsc_cfg->bits_per_component - 8));
			return *st_qp;
		}
	}
	else
		return *st_qp;
}