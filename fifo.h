#ifndef FIFO_H
#define FIFO_H

typedef struct fifo_s
{
	unsigned char *data;
	int size;
	int fullness;
	int read_ptr;
	int write_ptr;
	int max_fullness;
	int bits_added;
} fifo_t;

void fifo_init(fifo_t *fifo, int size);
void fifo_free(fifo_t *fifo);
int fifo_get_bits(fifo_t *fifo, int nbits, int sign_extend);
void fifo_put_bits(fifo_t *fifo, unsigned int d, int nbits);

#endif

