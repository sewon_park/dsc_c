#include "VLD.h"

void VLD::Init(dsc_cfg_t *dsc_cfg)
{
	InitQuantTable(dsc_cfg);
	prev_master_qp = 0;
	prevFirstFlat_dec = -1;
	numBits = 0;

	for (int i = 0; i<NUM_COMPONENTS; i++) {
		predicted_size[i] = 0;
	}
}

void VLD::BackUpQp(int masterQp)
{
	prev_master_qp = masterQp;
}

void VLD::VLDUnit(dsc_cfg_t *dsc_cfg, int groupCount, int masterQp, int *cpntBitDepth, int cpnt, unsigned char **byte_in_p, int &flatnessType, fifo_t *shifter, int& ich_sel, int& ich_sel_prev, int *ich_lookup_out, int *quantized_residuals, int* use_midpoint, int *rc_size_unit, int& firstFlat_dec, int& postMuxNumBits)
{
	int required_size[NUM_COMPONENTS];
	int size;
	int max_size;
	int prefix_value;
	int adj_predicted_size;
	int i;
	int alt_size_to_generate;
	int use_ich;
	int qlevel;
	int max_prefix;
	int midpoint_selected;
	int unit_to_read_fflag, unit_to_read_fpos;

	unit_to_read_fflag = 0;
	unit_to_read_fpos = 0;

	qlevel = MapQpToQlevel(masterQp, cpnt);

	adj_predicted_size = GetQpAdjPredSize(cpntBitDepth, masterQp, prev_master_qp, cpnt, predicted_size);

	// ich_sel_prev update & init ich_sel
	if (cpnt == 0)
	{
		ich_sel_prev = ich_sel;
		ich_sel = 0;
	}

	if ((cpnt == unit_to_read_fflag) && ((groupCount % GROUPS_PER_SUPERGROUP) == 3))
	{
		if (IsFlatnessInfoSent(dsc_cfg, masterQp))
		{
			if (GetBits(dsc_cfg, &postMuxNumBits, shifter, cpnt, 1, 0, *byte_in_p))
				prevFirstFlat_dec = 0;
			else
				prevFirstFlat_dec = -1;
		}
		else
			prevFirstFlat_dec = -1;
	}
	if ((cpnt == unit_to_read_fpos) && ((groupCount % GROUPS_PER_SUPERGROUP) == 0))
	{
		if (prevFirstFlat_dec >= 0)
		{
			flatnessType = 0;
			if (masterQp >= SOMEWHAT_FLAT_QP_THRESH)
				flatnessType = GetBits(dsc_cfg, &postMuxNumBits, shifter, cpnt, 1, 0, *byte_in_p);
			firstFlat_dec = GetBits(dsc_cfg, &postMuxNumBits, shifter, cpnt, 2, 0, *byte_in_p);
		}
		else
			firstFlat_dec = -1;
	}

	// Don't read other 2 components if we're in ICH mode
	if (ich_sel)
		return;	

	max_prefix = MaxResidualSize(cpntBitDepth, cpnt, masterQp) + (cpnt == 0) - adj_predicted_size; // +(CType==0) for escape code
	prefix_value = 0;
	while ((prefix_value < max_prefix) && !GetBits(dsc_cfg, &postMuxNumBits, shifter, cpnt, 1, 0, *byte_in_p))
		prefix_value++;

	if ((cpnt == 0) && ich_sel_prev)
		size = adj_predicted_size + prefix_value - 1;
	else
		size = adj_predicted_size + prefix_value;

	alt_size_to_generate = cpntBitDepth[0]+1-qlevel; //escape code size

	if (ich_sel_prev)
		use_ich = (prefix_value == 0);
	else
		use_ich = (size >= alt_size_to_generate);

	// ICH hit
	if ((cpnt == 0) && use_ich)  
	{
		ich_sel = 1;
		for (i = 0; i<PIXELS_PER_GROUP; ++i)
		{
			ich_lookup_out[i] = GetBits(dsc_cfg, &postMuxNumBits, shifter, i, ICH_BITS, 0, *byte_in_p);
		}
		rc_size_unit[0] = ICH_BITS + 1;
		rc_size_unit[1] = rc_size_unit[2] = ICH_BITS;

		return;
	}

	// *MODEL NOTE* MN_DEC_MPP_SELECT
	midpoint_selected = (size == cpntBitDepth[cpnt] - qlevel);
	use_midpoint[cpnt] = midpoint_selected;

	// Get bits from input bitstream
	max_size = 0;
	for (i = 0; i<SAMPLES_PER_UNIT; i++)
	{
		quantized_residuals[i] = GetBits(dsc_cfg, &postMuxNumBits, shifter, cpnt, size, 1, *byte_in_p);
		required_size[i] = FindResidualSize(quantized_residuals[i]);
		max_size = MAX(required_size[i], max_size);
	}

	if (midpoint_selected)
	{
		max_size = size;
		for (i = 0; i<SAMPLES_PER_UNIT; ++i)
			required_size[i] = size;
	}

	// rate control size uses max required size plus 1 (for prefix value of 0)
	rc_size_unit[cpnt] = max_size*SAMPLES_PER_UNIT + 1;

	// Predict size for next unit for this component
	// and store predicted size to state structure
	predicted_size[cpnt] = PredictSize(required_size);
}

int VLD::GetBits(dsc_cfg_t *dsc_cfg, int *postMuxNumBits, fifo_t *shifter, int unit, int nbits, int sign_extend, unsigned char *buf)
{
	numBits += nbits;

	if (dsc_cfg->muxing_mode == 0)
		return (getbits(nbits, buf, postMuxNumBits, sign_extend));

	return (fifo_get_bits(&(shifter[unit]), nbits, sign_extend));
}