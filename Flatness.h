#pragma once
#include "dsc_types.h"
#include "functions.h"

class Flatness : Functions
{
private:
	const int QuantDivisor[13] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };
	int prevIsFlat;			//< Flag indicating the previous group is flat
	int prevFlatnessType;   //< The flatnessType coded with the previous group
	int origIsFlat;			//< Flag indicating that original pixels are flat for this group

public:
//	int flatnessType;		//< 0 = somewhat flat; 1 = very flat

	void Init(dsc_cfg_t *dsc_cfg);

	void DecFlatnessDecision(int groupCount, int firstFlat_dec);
	int QpAdjustment(dsc_cfg_t *dsc_cfg, int master_qp, int flatnessType, int *st_qp);

};

