#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vdo.h"
#include "dsc_types.h"
#include "dsc_utils.h"
#include "dpx.h"
#include "utl.h"
#include "psnr.h"
#include "cmd_parse.h"
#include "dsc_codec.h"
#include "logging.h"

#define PATH_MAX 1024
#define MAX_OPTNAME_LEN 200
#define CFGLINE_LEN ((PATH_MAX) + MAX_OPTNAME_LEN)
#define RANGE_CHECK(s,a,b,c) { if(((a)<(b))||((a)>(c))) { UErr("%s out of range, needs to be between %d and %d\n",s,b,c); } }


class CodecMain
{
public:
	DSCCodec dsc_algorithm;

	int      help;
	char fn_i[PATH_MAX + 1];
	char fn_o[PATH_MAX + 1];
	char fn_log[PATH_MAX + 1];
	char filepath[PATH_MAX + 1];
	char option[PATH_MAX + 1];
	int rcModelSize;
	float bitsPerPixel;
	int bitsPerComponent;
	int enable422;
	int lineBufferBpc;
	int bpEnable;
	int initialDelay;
	int sliceWidth;
	int sliceHeight;
	int firstLineBpgOfs;
	int initialFullnessOfs;
	int useYuvInput;
	int function;
	int *rcOffset;
	int *rcMinQp;
	int *rcMaxQp;
	int *rcBufThresh;
	int tgtOffsetHi;
	int tgtOffsetLo;
	int rcEdgeFactor;
	int quantIncrLimit0;
	int quantIncrLimit1;
	int rbSwap;
	int rbSwapOut;
	int dpxBugsOverride;
	int dpxPadLineEnds;
	int dpxWriteBSwap;
	int flatnessMinQp;
	int flatnessMaxQp;
	int flatnessDetThresh;
	int enableVbr;
	int muxingMode;
	int muxWordSize;

	cmdarg_t cmd_args[40];
	

	CodecMain();
	~CodecMain();
	void set_defaults(void);
	int parse_cfgfile(char* fn, cmdarg_t *cmdargs);
	int assign_line(char* line, cmdarg_t *cmdargs);
	void usage(void);
	int process_args(int argc, char *argv[], cmdarg_t *cmdargs);
	void split_base_and_ext(char *infname, char *base_name, char **extension);
	void write_dsc_data(unsigned char **bit_buffer, int nbytes, FILE *fp, int vbr_enable, int slices_per_line, int slice_height, int **sizes);
	int read_dsc_data(unsigned char **bit_buffer, int nbytes, FILE *fp, int vbr_enable, int slices_per_line, int slice_height);

	#ifdef WIN32
	int codec_main(int argc, char *argv[]);
};
	#else
};
	int main(int argc, char *argv[]);
	#endif


