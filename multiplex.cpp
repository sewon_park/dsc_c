#include <stdio.h>
#include <stdlib.h>

#include "fifo.h"
#include "dsc_types.h"
#include "dsc_utils.h"
#include "multiplex.h"

void MUX::Init(dsc_cfg_t *dsc_cfg, int *cpntBitDepth)
{
	for (int i = 0; i < NUM_COMPONENTS; i++) {
		fifo_init(&(shifter[i]), (dsc_cfg->mux_word_size + MAX_SE_SIZE + 7) / 8);
		switch (i) {
		case 0:
			max_se_size[i] = cpntBitDepth[i] * 4 + 4;
			break;
		default:
			max_se_size[i] = cpntBitDepth[i] * 4;  // prefix omitted
			break;
		}
	}
}

//! Process a single group through the FIFO's, and insert 0 - 3 mux words in shifters
void MUX::ProcessGroupDec(dsc_cfg_t *dsc_cfg, int *postMuxNumBits, unsigned char *buf)
{
	unsigned char d;

	// Check to see if mux words are needed and feed the FIFO's
	for (int cpnt = 0; cpnt<NUM_COMPONENTS; ++cpnt) {
		if (shifter[cpnt].fullness < max_se_size[cpnt]) {
			for (int j = 0; j<(dsc_cfg->mux_word_size / 8); ++j) {
				d = getbits(8, buf, postMuxNumBits, 0);
				fifo_put_bits(&shifter[cpnt], d, 8);
			}
		}
	}
}
