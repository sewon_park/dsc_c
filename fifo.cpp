#include <stdio.h>
#include <stdlib.h>
#include "fifo.h"

//! Initialize a FIFO object
/*! \param fifo		 Pointer to FIFO data structure
    \param size		 Specifies FIFO size in bytes */
void fifo_init(fifo_t *fifo, int size)
{
	fifo->data = (unsigned char *)malloc(sizeof(unsigned char) * size);
	fifo->size = size*8;
	fifo->fullness = 0;
	fifo->read_ptr = fifo->write_ptr = 0;
	fifo->max_fullness = 0;
	fifo->bits_added = 0;
}


//! Free a FIFO object
/*! \param fifo		Pointer to FIFO data structure */
void fifo_free(fifo_t *fifo)
{
	free(fifo->data);
}


//! Get bits from a FIFO
/*! \param fifo		Pointer to FIFO data structure 
    \param n		Number of bits to retrieve
	\param sign_extend Flag indicating to extend sign bit for return value
	\return			Value from FIFO */
int fifo_get_bits(fifo_t *fifo, int n, int sign_extend)
{
	unsigned int d = 0;
	int i, pad = 0;
	unsigned char b;
	int sign = 0;

	if (fifo->fullness < n)
	{
		printf("FIFO underflow!\n");
		exit(1);
	}
	if (fifo->fullness < n)
	{
		pad = n - fifo->fullness;
		n = fifo->fullness;
	}

	for (i=0; i<n; ++i)
	{
		b = (fifo->data[fifo->read_ptr/8] >> (7-(fifo->read_ptr%8))) & 1;
		if (i==0) sign=b;
		d = (d<<1) + b;
		fifo->fullness --;
		fifo->read_ptr ++;
		if (fifo->read_ptr >= fifo->size)
			fifo->read_ptr = 0;
	}

	if (sign_extend && sign)
	{
		int mask;
		mask = (1<<n)-1;
		d |= (~mask);
	}
	return (d << pad);
}


//! Put bits into a FIFO
/*! \param fifo		Pointer to FIFO data structure
	\param d		Value to add to FIFO
    \param n		Number of bits to add to FIFO */
void fifo_put_bits(fifo_t *fifo, unsigned int d, int nbits)
{
	int i;
	unsigned char b;

	if (fifo->fullness + nbits > fifo->size)
	{
		printf("FIFO overflow!\n");
		exit(1);
	}

	fifo->bits_added += nbits;
	fifo->fullness += nbits;
	for (i=0; i<nbits; ++i)
	{
		b = (d >> (nbits-i-1)) & 1;
		if (!b)
			fifo->data[fifo->write_ptr/8] &= ~(1<<(7-(fifo->write_ptr%8)));
		else
			fifo->data[fifo->write_ptr/8] |= (1<<(7-(fifo->write_ptr%8)));
		fifo->write_ptr++;
		if (fifo->write_ptr >= fifo->size)
			fifo->write_ptr = 0;
	}
	if (fifo->fullness > fifo->max_fullness)
		fifo->max_fullness = fifo->fullness;
}

