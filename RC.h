#pragma once
#include "dsc_types.h"

class RC
{
private:
	int prev_qp;		  		//< QP for previous group from RC
	int rc_size_group;		//< Sum of RcSizeUnits for previous group
//	int bpgFracAccum;		//< Accumulator for fractional bits/group
	int bpg_offset;
	int throttleFrac;		//< Fractional portion of RC offset
	int currentScale;		//< Current scale factor used for RC model
	int scaleAdjustCounter;  //< Counter used to compute when to adjust scale factor
	int prevRange;         //< RC range for previous group
	int prevPixelCount;		//< Number of pixels that have been processed as of the previous group
	int rcOffsetClampEnable; //< Set to true after rcXformOffset falls below final_offset - rc_model_size
	int scaleIncrementStart; //< Flag indicating that the scale increment has started
	int chunkPixelTimes;    //< Number of pixels that have been generated for the current slice line
	int *chunkSizes;        //< For encoders, stores the chunk sizes for each chunk in slice multiplexing

public:
	int rcXformOffset;		//< Offset used for RC model (to get rcXformOffset from spec, subtract rc_model_size from this value)
	int scale;
	int codedGroupSize;		//< Size of previous group in bits
	int prevNumBits;		//< Number of bits at end of previous group
	int st_qp;				//< QP from RC
	int numBitsChunk;		//< Number of bits output for the current chunk
	int bufferFullness;		//< Current buffer fullness
	int pixelCount;         //< Number of pixels that have been processed
	int bitsClamped;        //< Number of bits clamped by buffer level tracker in VBR mode


	void Init(dsc_cfg_t *dsc_cfg, int *chunk_sizes);
	void UpdatePrevNumBIts(int NumBits);

	void RcFunction(dsc_cfg_t *dsc_cfg, int first_line_flag, int group_count, int group_size, int* rc_size_unit, int numBits);

	void UpdateBufferFullness_DEC(dsc_cfg_t *dsc_cfg, int numBits);
	void CalcFullnessOffset(dsc_cfg_t *dsc_cfg, int first_line_flag, int group_count);
	void RateControl(dsc_cfg_t *dsc_cfg, int group_size, int* rc_size_unit);

	void RemoveBitsEncoderBuffer_DEC(dsc_cfg_t *dsc_cfg, int group_size);

};

