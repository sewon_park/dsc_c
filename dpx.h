#ifndef DPX_H
#define DPX_H

#include "vdo.h"

#define DPX_ERROR_UNRECOGNIZED_CHROMA  -1
#define DPX_ERROR_UNSUPPORTED_BPP      -2
#define DPX_ERROR_NOT_IMPLEMENTED      -3
#define DPX_ERROR_MISMATCH             -4
#define DPX_ERROR_BAD_FILENAME         -5
#define DPX_ERROR_CORRUPTED_FILE       -6
#define DPX_ERROR_MALLOC_FAIL          -7

enum DPX_VER_e { STD_DPX_VER = 0, DVS_DPX_VER = 1};

format_t determine_field_format(char* file_name);
int      ends_in_percentd(char* str, int length);

int      dpx_write(char *fname, pic_t *p, int pad_line_ends, int bswap);
int      dpx_read(char *fname, pic_t **p, int dpx_bugs);
void     set_dpx_colorspace(int color);

int      dpx_read_hl(char *fname, pic_t **p, int *high, int *low, int dpx_bugs);

int      write_dpx_ver(char *fname, pic_t *p, int ar1, int ar2, int frameno, int seqlen, float framerate, int interlaced, int bpp, int dvs_compat, int pad_line_ends, int bswap);
int      write_dpx(char *fname, pic_t *p, int ar1, int ar2, int frameno, int seqlen, float framerate, int interlaced, int bpp, int pad_line_ends, int bswap);
int      read_dpx(char *fname, pic_t **p, int *ar1, int *ar2, int *frameno, int *seqlen, float *framerate, int *interlaced, int *bpp, int dpx_bugs);

#endif
