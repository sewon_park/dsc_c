#CC = gcc
CC = g++
GCCVER =
#CC = g++
DEFINES = #JFLAGS = -std=c99 -g -Wall
#JFLAGS = -std=c99 -O3 -Wall
CFLAGS = -std=c99 -O3 -Wall -g
# =================================================================================

dsc_SRCS = \
	dsc_codec.cpp \
	dsc_utils.cpp \
	cmd_parse.cpp \
	codec_main.cpp \
	dpx.cpp \
	fifo.cpp \
	logging.cpp \
	multiplex.cpp \
	cvt.cpp \
	Flatness.cpp \
	functions.cpp \
	ICH.cpp \
	LineBuffer.cpp \
	Predictor.cpp \
	psnr.cpp \
	RC.cpp \
	utl.cpp \
	VLD.cpp \

dsc_OBJS = ${dsc_SRCS:.cpp=.o}

# ----------------------------------------------------------------

dsc: $(dsc_OBJS)
	$(CC) $(dsc_OBJS) $(DEFINES  -lm -o dsc

# ----------------------------------------------------------------
.cpp.o:
	$(CC) $(CFLAGS) $(DEFINES) -c $*.cpp 

.cpp.ln:
	lint -c $*.cpp 

% : %.cpp vdo.h utl.cpp utl.h dpx.h
	g++ -O -o $@ $@.c utl.cpp dpx.cpp -lm -W -Wall -std=c99 $(CFLAGS)

all: dsc

clean:
	rm -f *.o
	rm -f dsc
