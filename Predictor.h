#pragma once
#include "dsc_types.h"
#include "functions.h"
#include "vdo.h"
#include "ICH.h"

class Predictor : Functions
{
private:
	int bpCount;			//< Number of times in a row block prediction had the lowest cost
	int pred_err[NUM_COMPONENTS][BP_RANGE];   //< Errors for all pred types for each component
	int midpoint_recon[MAX_UNITS_PER_GROUP][SAMPLES_PER_UNIT];  //< Reconstructed samples assuming midpoint prediction is selected
	int left_recon[NUM_COMPONENTS];		//< Previous group's rightmost reconstructed samples (used for midpoint prediction)
	int lastEdgeCount;		//< How long ago we saw the last edge (for BP)
	int lastErr[NUM_COMPONENTS][BP_SIZE][BP_RANGE];  //< 3-pixel SAD's for each of the past 3 3-pixel-wide prediction blocks for each BP offset
	int edgeDetected;       //< Was an edge detected for BP
	int range[NUM_COMPONENTS];
	int quantized_residual_mid[MAX_UNITS_PER_GROUP][SAMPLES_PER_UNIT];

public:
	int max_error[NUM_COMPONENTS];  //< Max error for each component using DPCM
	int max_mid_error[NUM_COMPONENTS];  //< Max error for each component using MPP
	int master_qp_enc;

	const int QuantDivisor[13] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };

	PRED_TYPE *prevLinePred;  //< BP selection decsion buffer (since model calculates BP offset one line ahead of time)


	//wrapping function
	void Init(dsc_cfg_t *dsc_cfg);
	void BeforeFlatness_DEC(dsc_cfg_t *dsc_cfg, int vPos, int hPos, int qp, int *cpntBitDepth, int cpnt, int sampModCnt, int **currLine, int **prevLine,
						int* use_midpoint, int IchSelected, unsigned int *p, pic_t *op);
	void SelectValue(int hPos, int group_size, int IchSelected, unsigned int ich[PIXELS_PER_GROUP][NUM_COMPONENTS], int pred[PIXELS_PER_GROUP][NUM_COMPONENTS], int currLine[PIXELS_PER_GROUP][NUM_COMPONENTS]);
	void Prediction(int firsit_line_flag, int group_num, int group_size, int qp, int *cpntBitDepth, int** currLine, int **prevLine,
		int* use_midpoint, int quantized_residual[MAX_UNITS_PER_GROUP][SAMPLES_PER_UNIT], int recon_x[PIXELS_PER_GROUP][NUM_COMPONENTS],FILE* log);

	void UpdateLeftRecon_DEC(dsc_cfg_t *dsc_cfg, int leftest_pixel[NUM_COMPONENTS]);

	// default function
	int SamplePredict_DEC(int quantized_residual[MAX_UNITS_PER_GROUP][SAMPLES_PER_UNIT], int* cpntBitDepth, int* prevLine, int currLine[BUFFER_FIX], int group_num, int pixel, PRED_TYPE predType, int qLevel, int cpnt);
	void BlockPredSearch_DEC(int* cpntBitDepth, int linebuf_depth, int block_pred_enable, int currLine[NUM_COMPONENTS][BUFFER_FIX], int gruop_num, int group_size,FILE* log_err, FILE* log_edge, FILE* log_bpcount, FILE* log_bpsad);
	int FindMidpoint(int* cpntBitDepth, int* leftRecon, int cpnt, int qlevel);

private:
	const int QuantOffset[13] = { 0, 0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047 };
};
