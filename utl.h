#ifndef _UTL_H
#define _UTL_H

#include <stdio.h>
#include "vdo.h"

void *palloc(int w, int h);
pic_t *pcreate(int format, int color, int chroma, int w, int h);
void *pdestroy(pic_t *p);

void yuv_444_422(pic_t *ip, pic_t *op);
void yuv_422_444(pic_t *ip, pic_t *op);
void rgb2yuv(pic_t *ip, pic_t *op);
void yuv2rgb(pic_t *ip, pic_t *op);

void convertbits(pic_t *p, int newbits);
int ppm_read(char *fname, pic_t **pic);
int ppm_write(char *fname, pic_t *pic);

#endif
