#ifndef __DSC_UTILS_H_
#define __DSC_UTILS_H_

#include "utl.h"
#include "dsc_types.h"
//#define REDUCE_CHROMA_12BPC

void putbits(int val, int size, unsigned char *buf, int *bit_count);
int getbits(int size, unsigned char *buf, int *bit_count, int sign_extend);

void *pcreateb(int format, int color, int chroma, int w, int h, int bits);

void simple422to444(pic_t *ip, pic_t *op);
void simple444to422(pic_t *ip, pic_t *op);

void parse_pps(unsigned char *buf, dsc_cfg_t *dsc_cfg);
void write_pps(unsigned char *buf, dsc_cfg_t *dsc_cfg);

int ceil_log2(int val);

#endif // __DSC_UTILS_H_
