#pragma once
#include <assert.h>
#include "dsc_utils.h"
#include "functions.h"

class VLD :
	public Functions
{
private:

public:
//	int firstFlat_dec;
	int prev_master_qp;			//< QP used for the previous group
	int predicted_size[NUM_COMPONENTS];  //< Predicted sizes for next DSU code 
	int prevFirstFlat_dec;
	int numBits;			//< Number of bits read/written

	void Init(dsc_cfg_t *dsc_cfg);
	void BackUpQp(int masterQp);


	void VLDUnit(dsc_cfg_t *dsc_cfg, int groupCount, int masterQp, int *cptnBitDepth, int cpnt, unsigned char **byte_in_p, int &flatnessType,
		fifo_t *shifter, int& ich_sel, int& ich_sel_prev, int *ich_lookup_out, int *quantized_residuals, int* use_midpoint, int *rc_size_unit, int& firstFlat_dec, int& postMuxNumBits);
	int GetBits(dsc_cfg_t *dsc_cfg, int *postMuxNumBits, fifo_t *shifter, int unit, int nbits, int sign_extend, unsigned char *buf);

};

