#include "RC.h"
#include "logging.h"
#include "utl.h"

void RC::Init(dsc_cfg_t *dsc_cfg, int *chunk_sizes)
{
	st_qp = 0;
	prev_qp = 0;
	rc_size_group = 0;
	rcXformOffset = dsc_cfg->initial_offset;
	throttleFrac = 0;
	prevRange = 0;
	numBitsChunk = 0;
	prevNumBits = 0;
	bufferFullness = 0;
	pixelCount = 0;
	prevPixelCount = 0;
	rcOffsetClampEnable = 0;
	scaleIncrementStart = 0;
	bitsClamped = 0;
	chunkPixelTimes = 0;
	chunkSizes = chunk_sizes;
}

void RC::UpdatePrevNumBIts(int NumBits)
{
	prevNumBits = NumBits;
}

void RC::RcFunction(dsc_cfg_t *dsc_cfg, int first_line_flag, int group_count, int group_size, int* rc_size_unit, int numBits)
{
	UpdateBufferFullness_DEC(dsc_cfg, numBits);
	// Calculate scale & offset for RC
	CalcFullnessOffset(dsc_cfg, first_line_flag, group_count);
	// Do rate control
	RateControl(dsc_cfg, group_size, rc_size_unit);
}

void RC::UpdateBufferFullness_DEC(dsc_cfg_t * dsc_cfg, int numBits)
{
	codedGroupSize = numBits - prevNumBits;
	bufferFullness += codedGroupSize;

	if (bufferFullness > dsc_cfg->rcb_bits) {
		// This check may actually belong after tgt_bpg has been subtracted
		printf("The buffer model has overflowed.  This probably occurred due to an attempt to decode an invalid DSC stream.\n\n");
		printf("ERROR: RCB overflow; size is %d, tried filling to %d\n", dsc_cfg->rcb_bits, bufferFullness);
		exit(1);
	}
}

// Rate control function
void RC::RateControl(dsc_cfg_t *dsc_cfg, int group_size, int* rc_size_unit)
{
	int i;
	int prev_fullness;
	int rcSizeGroup_;
	int rcTgtBitsGroup;
	int overflowAvoid;
	int min_QP;
	int max_QP;
	int tgtMinusOffset;
	int tgtPlusOffset;
	dsc_range_cfg_t *range_cfg;
	int rcSizeGroupPrev = rc_size_group;
	int prevQp_ = st_qp;
	int prev2Qp = prev_qp;
	int rcModelBufferFullness;
	int bpg;
	int incr_amount;
	int selected_range;
	int stQp_;
	int curQp;
	int throttle_offset = rcXformOffset;

	prev_fullness = bufferFullness;

	// Function to remove one pixel's worth of bits from the encoder buffer model
    RemoveBitsEncoderBuffer_DEC(dsc_cfg, group_size);

	// Add up estimated bits for the Group, i.e. as if VLC sample size matched max sample size
	rcSizeGroup_ = 0;
	for (i = 0; i < NUM_COMPONENTS; i++)
		rcSizeGroup_ += rc_size_unit[i];

	// Set target number of bits per Group according to buffer fullness
	range_cfg = dsc_cfg->rc_range_parameters;
	throttle_offset -= dsc_cfg->rc_model_size;

	// *MODEL NOTE* MN_RC_XFORM
	rcModelBufferFullness = (scale * (bufferFullness + throttle_offset)) >> RC_SCALE_BINARY_POINT;

	// Pick the correct range
	// *MODEL NOTE* MN_RC_LONG_TERM
	for (i = NUM_BUF_RANGES - 1; i>0; --i)
	{
		overflowAvoid = (bufferFullness + throttle_offset > OVERFLOW_AVOID_THRESHOLD);
		if ((rcModelBufferFullness > dsc_cfg->rc_buf_thresh[i - 1] - dsc_cfg->rc_model_size))
			break;
	}

	if (rcModelBufferFullness > 0)
	{
		printf("The RC model has overflowed.  To address this issue, please adjust the\n");
		printf("min_QP and max_QP higher for the top-most ranges, or decrease the rc_buf_thresh\n");
		printf("for those ranges.\n");
		exit(1);
	}

	// Add a group time of delay to RC calculation
	selected_range = prevRange;
	prevRange = i;

	// *MODEL NOTE* MN_RC_SHORT_TERM
	bpg = (dsc_cfg->bits_per_pixel * group_size);
	bpg = (bpg + 8) >> 4;
	rcTgtBitsGroup = MAX(0, bpg + range_cfg[selected_range].range_bpg_offset + bpg_offset);
	min_QP = range_cfg[selected_range].range_min_qp;
	max_QP = range_cfg[selected_range].range_max_qp;

	tgtMinusOffset = MAX(0, rcTgtBitsGroup - dsc_cfg->rc_tgt_offset_lo);
	tgtPlusOffset = MAX(0, rcTgtBitsGroup + dsc_cfg->rc_tgt_offset_hi);
	incr_amount = (codedGroupSize - rcTgtBitsGroup) >> 1;

	if (rcSizeGroup_ == PIXELS_PER_GROUP)
	{
		stQp_ = MAX(min_QP / 2, prevQp_ - 1);
	}
	else if ((codedGroupSize < tgtMinusOffset) && (rcSizeGroup_ < tgtMinusOffset))
	{
		stQp_ = MAX(min_QP, (prevQp_ - 1));
	}
	// avoid increasing QP immediately after edge
	else if ((bufferFullness >= 64) && codedGroupSize > tgtPlusOffset)  // over budget - increase qp
	{
		curQp = MAX(prevQp_, min_QP);
		if (prev2Qp == curQp)   // 2nd prev grp == prev grp 
		{
			if ((rcSizeGroup_ * 2) < (rcSizeGroupPrev*dsc_cfg->rc_edge_factor))
				stQp_ = MIN(max_QP, (curQp + incr_amount));
			else
				stQp_ = curQp;
		}
		else if (prev2Qp < curQp)
		{
			if (((rcSizeGroup_ * 2) < (rcSizeGroupPrev*dsc_cfg->rc_edge_factor) && (curQp < dsc_cfg->rc_quant_incr_limit0)))
				stQp_ = MIN(max_QP, (curQp + incr_amount));
			else
				stQp_ = curQp;
		}
		else if (curQp < dsc_cfg->rc_quant_incr_limit1)
		{
			stQp_ = MIN(max_QP, (curQp + incr_amount));
		}
		else
		{
			stQp_ = curQp;
		}
	}
	else
		stQp_ = prevQp_;

	if (overflowAvoid)
		stQp_ = range_cfg[NUM_BUF_RANGES - 1].range_max_qp;

	rcSizeGroupPrev = rcSizeGroup_;

	rc_size_group = rcSizeGroup_;
	st_qp = stQp_;
	prev_qp = prevQp_;

	if (bufferFullness > dsc_cfg->rcb_bits)
	{
		printf("The buffer model has overflowed.  This probably occurred due to an error in the\n");
		printf("rate control parameter programming or an attempt to decode an invalid DSC stream.\n\n");
		printf("ERROR: RCB overflow; size is %d, tried filling to %d\n", dsc_cfg->rcb_bits,bufferFullness);
		printf("  previous level: %5d\n", prev_fullness);
		printf("  target BPP:     %5d\n", dsc_cfg->bits_per_pixel >> 4);
		printf("  new level:      %5d\n", bufferFullness);
		printf("Range info\n");
		printf("  range min_qp:     %d\n", range_cfg->range_min_qp);
		printf("  range max_qp:     %d\n", range_cfg->range_max_qp);
		printf("  range bpg_offset: %d\n", range_cfg->range_bpg_offset);
		range_cfg--;
		printf("Previous range info\n");
		printf("  range min_qp:     %d\n", range_cfg->range_min_qp);
		printf("  range max_qp:     %d\n", range_cfg->range_max_qp);
		exit(1);
	}

	//check buffer underflow
	if (bufferFullness < 0) {
		if (dsc_cfg->vbr_enable) {
			bitsClamped += -bufferFullness;
			bufferFullness = 0;
		}
		else {
			printf("The buffer model encountered an underflow.  This may have occurred due to\n");
			printf("an excessively high constant bit rate or due to an attempt to decode an\n");
			printf("invalid DSC stream.\n");
			exit(1);
		}
	}
}

// currently this module doesn't support bpg fraction accumulation
void RC::RemoveBitsEncoderBuffer_DEC(dsc_cfg_t *dsc_cfg, int group_size)
{
    int delay_check;
    int delay_product;
    int chunk_check;
    int chunk_product;

    pixelCount += group_size;

    delay_check = pixelCount - dsc_cfg->initial_xmit_delay;

    if (delay_check >= 0) {
        switch (delay_check) {
        case 0:
            delay_product = 1;
            break;
        case 1: 
            delay_product = 2;
            break;
        default: 
            delay_product = group_size;
            break;
        }
        bufferFullness -= (dsc_cfg->bits_per_pixel >> 4) * delay_product;
        numBitsChunk += (dsc_cfg->bits_per_pixel >> 4) * delay_product;
        chunkPixelTimes += delay_product;

        chunk_check = chunkPixelTimes - dsc_cfg->slice_width;

        if (chunk_check >= 0) {
            switch (chunk_check) {
            case 0:
                chunk_product = 0;
                break;
            case 1:
                chunk_product = 1;
                break;
            case 2:
                chunk_product = 2;
                break;
            }
            if (dsc_cfg->vbr_enable) {
                bufferFullness -= 8 - (((numBitsChunk - (dsc_cfg->bits_per_pixel >> 4) * chunk_product) - bitsClamped) & 0x7);
                bitsClamped = 0;
            }
            // CBR mode
            else {
                bufferFullness -= (dsc_cfg->chunk_size * 8 - (numBitsChunk - (dsc_cfg->bits_per_pixel >> 4) * chunk_product));
            }
            numBitsChunk = (dsc_cfg->bits_per_pixel >> 4) * chunk_product;
            chunkPixelTimes = chunk_product;
        }
    }
}

// Calculate offset & scale values for rate control
void RC::CalcFullnessOffset(dsc_cfg_t *dsc_cfg, int first_line_flag, int group_count)
{
	// positive offset takes bits away, negative offset provides more bits
	int current_bpg_target;
	int unity_scale;
	int increment;

	// *MODEL NOTE* MN_CALC_SCALE_OFFSET
	unity_scale = (1 << RC_SCALE_BINARY_POINT);

	if (group_count == 0)
	{
		currentScale = dsc_cfg->initial_scale_value;
		scaleAdjustCounter = 1;
	}
	else if (first_line_flag && (currentScale > unity_scale))  // Reduce scale at beginning of slice
	{
		scaleAdjustCounter++;
		if (scaleAdjustCounter >= dsc_cfg->scale_decrement_interval)
		{
			scaleAdjustCounter = 0;
			currentScale--;
		}
	}
	else if (scaleIncrementStart)
	{
		scaleAdjustCounter++;
		if (scaleAdjustCounter >= dsc_cfg->scale_increment_interval)
		{
			scaleAdjustCounter = 0;
			currentScale++;
		}
	}

	// Account for first line boost
	if (first_line_flag)
	{
		current_bpg_target = dsc_cfg->first_line_bpg_ofs;
		increment = -(dsc_cfg->first_line_bpg_ofs << OFFSET_FRACTIONAL_BITS);
	}
	else
	{
		current_bpg_target = -(dsc_cfg->nfl_bpg_offset >> OFFSET_FRACTIONAL_BITS);
		increment = dsc_cfg->nfl_bpg_offset;
	}

	// Account for initial delay boost
	if (pixelCount < dsc_cfg->initial_xmit_delay)
	{
		int num_pixels;

		if (pixelCount == 0)
			num_pixels = PIXELS_PER_GROUP;
		else
			num_pixels = pixelCount - prevPixelCount;
		num_pixels = MIN(dsc_cfg->initial_xmit_delay - pixelCount, num_pixels);
		increment -= (dsc_cfg->bits_per_pixel * num_pixels) << (OFFSET_FRACTIONAL_BITS - 4);
	}
	else
	{
		if (dsc_cfg->scale_increment_interval && !scaleIncrementStart && !first_line_flag && (rcXformOffset > 0))
		{
			currentScale = 9;
			scaleAdjustCounter = 0;
			scaleIncrementStart = 1;
		}
	}

	prevPixelCount = pixelCount;

	current_bpg_target -= (dsc_cfg->slice_bpg_offset >> OFFSET_FRACTIONAL_BITS);
	increment += dsc_cfg->slice_bpg_offset;

	throttleFrac += increment;
	rcXformOffset += throttleFrac >> OFFSET_FRACTIONAL_BITS;
	throttleFrac = throttleFrac & ((1 << OFFSET_FRACTIONAL_BITS) - 1);

	if (rcXformOffset < dsc_cfg->final_offset)
		rcOffsetClampEnable = 1;

	if (rcOffsetClampEnable)
		rcXformOffset = MIN(rcXformOffset, dsc_cfg->final_offset);

	scale = currentScale;
	bpg_offset = current_bpg_target;
}
