#ifndef MULTIPLEX_H
#define MULTIPLEX_H
#include "dsc_types.h"

class MUX
{
private:
	int max_se_size[NUM_COMPONENTS];  //< process group decode

public:
	fifo_t shifter[NUM_COMPONENTS];		//< Decoder funnel shifter


	void Init(dsc_cfg_t *dsc_cfg, int *cpntBitDepth);

	void ProcessGroupDec(dsc_cfg_t *dsc_cfg, int *postMuxNumBits, unsigned char *buf);
};
#endif
