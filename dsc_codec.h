#ifndef __DSC_H_
#define __DSC_H_

#define Fmt_444	1
#define Fmt_422	0

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <memory.h>
#include <direct.h>
#include "vdo.h"
#include "dsc_types.h"
#include "VLD.h"
#include "cvt.h"
#include "Predictor.h"
#include "ICH.h"
#include "multiplex.h"
#include "RC.h"
#include "Flatness.h"
#include "LineBuffer.h"

#endif // __DSC_H_

//#define LOG

class DSCCodec
{
private:
	VLD vld;
	CVT cvt;
	Predictor pred;
	ICH ich;
	MUX mux;
	RC rc;
	Flatness flatness;
	LineBuffer prev_line;
	LineBuffer curr_line;
	wire_t wire;

#ifdef LOG
	//  Log file 
	FILE* log_front_vld_rate;
    FILE* log_pred_mid;
    FILE* log_pred_signal;
    FILE* log_pred_residual;
    FILE* log_pred_prev;
    FILE* log_pred_curr;
    FILE* log_pred_predmode;
    FILE* log_pred_reconstruct;
    FILE* log_pred_leftrecon;
    FILE* log_pred_ichuse;
    FILE* log_pred_pred_val;
    //log ICH;
    FILE* log_ich_index;
    FILE* log_ich_value;
    FILE* log_ich;
    FILE* log_ich_prev_line;
    FILE* log_ich_sel;
    FILE* log_ich_recon;
    FILE* log_selector_ichsel;
    FILE* log_selector_reference;
    //log search log
    FILE* log_search_edge;
    FILE* log_search_err;
    FILE* log_search_bpcount;
    FILE* log_search_bpsad;
#else
	FILE* log_dummy;
#endif

	int group_count;
	int group_count_line; // for decoder
	int group_size; // for decoder
	bool last_group;
	int vPos;
	int cpntBitDepth[NUM_COMPONENTS];  //< Bit depth for each component
	int postMuxNumBits;    //< Number of bits read/written post-mux

public:
	void Init(dsc_cfg_t *dsc_cfg, int *chunk_sizes);

	int DSC_Encode(dsc_cfg_t* bdc_cfg, pic_t *p_in, pic_t* p_out, unsigned char* cmpr_buf, pic_t **temp_pic, int *chunk_sizes);
	void DSC_Decode(dsc_cfg_t* bdc_cfg, pic_t* p_out, unsigned char* cmpr_buf, pic_t **temp_pic);
	
	//! Main DSC encoding and decoding algorithm
	int DSC_Algorithm(int isEncoder, dsc_cfg_t* dsc_cfg, pic_t* ip, pic_t* op, unsigned char* cmpr_buf, pic_t **temp_pic, int *chunk_sizes);
	
	void OutToImageBuffer(dsc_cfg_t* dsc_cfg, int group_num, int group_size, pic_t* op);
};		

