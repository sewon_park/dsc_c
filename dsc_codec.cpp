#include "dsc_codec.h"

//! Wrapper function for encode
/*! \param dsc_cfg   DSC configuration structure
\param p_in      Input picture
\param p_out     Output picture
\param cmpr_buf  Pointer to empty buffer to hold compressed bitstream
\param temp_pic  Array of two pictures to use as temporary storage for YCoCg conversions
\return          Number of bits in the resulting compressed bitstream */
int DSCCodec::DSC_Encode(dsc_cfg_t *dsc_cfg, pic_t *p_in, pic_t *p_out, unsigned char *cmpr_buf, pic_t **temp_pic, int *chunk_sizes)
{
	return DSC_Algorithm(1, dsc_cfg, p_in, p_out, cmpr_buf, temp_pic, chunk_sizes);
}

//! Wrapper function for decode
/*! \param dsc_cfg   DSC configuration structure
\param p_out     Output picture
\param cmpr_buf  Pointer to buffer containing compressed bitstream
\param temp_pic  Array of two pictures to use as temporary storage for YCoCg conversions */
void DSCCodec::DSC_Decode(dsc_cfg_t *dsc_cfg, pic_t *p_out, unsigned char *cmpr_buf, pic_t **temp_pic)
{
	DSC_Algorithm(0, dsc_cfg, p_out, p_out, cmpr_buf, temp_pic, NULL);
}

void DSCCodec::Init(dsc_cfg_t *dsc_cfg, int *chunk_sizes)
{
	group_count = 0;
	group_count_line = 0;
	group_size = 0;
	last_group = false;
	vPos = 0;
	postMuxNumBits = 0;

	wire = { 0 };

	for (int i = 0; i<NUM_COMPONENTS; i++) {
		cpntBitDepth[i] = dsc_cfg->bits_per_component;
	}
	if (dsc_cfg->convert_rgb) {
		cpntBitDepth[1]++;
		cpntBitDepth[2]++;
	}

	vld.Init(dsc_cfg);
	cvt.Init();
	pred.Init(dsc_cfg);
	ich.Init(dsc_cfg);
	mux.Init(dsc_cfg, cpntBitDepth);
	rc.Init(dsc_cfg, chunk_sizes);
	flatness.Init(dsc_cfg);

#ifdef LOG
	//log file
	log_front_vld_rate = fopen("log_total.txt", "a");
	//pred 
    _mkdir("log_pred");
	log_pred_signal	  = fopen("log_pred/log_pred_signal.txt",       "a");
	log_pred_mid	  = fopen("log_pred/log_pred_midpoint.txt",     "a");
	log_pred_residual = fopen("log_pred/log_pred_residual.txt",     "a");
	log_pred_prev	  = fopen("log_pred/log_pred_prev.txt",         "a");
	log_pred_curr	  = fopen("log_pred/log_pred_curr.txt",         "a");
	log_pred_predmode = fopen("log_pred/log_pred_predmode.txt",     "a");
	log_pred_reconstruct = fopen("log_pred/log_pred_reference.txt", "a");
	log_pred_leftrecon = fopen("log_pred/log_pred_leftrecon.txt", "a");
	log_pred_ichuse = fopen("log_pred/log_pred_predichuse.txt", "a");
	log_pred_pred_val = fopen("log_pred/log_pred_prediction_val.txt", "a");
	//
    _mkdir("log_ich");
    log_ich_index	       = fopen("log_ich/log_ich_index.txt"      , "a");
	log_ich_value	       = fopen("log_ich/log_ich_reference.txt"  , "a");
	log_ich_sel		       = fopen("log_ich/log_ich_sel.txt"        , "a");
	log_ich_recon	       = fopen("log_ich/log_ich_recon.txt"      , "a");
	log_ich_prev_line      = fopen("log_ich/log_ich_prev_line.txt"  , "a");
	//selector               
    _mkdir("log_selector");
    log_selector_ichsel    = fopen("log_selector/log_selector_selection.txt", "a");
	log_selector_reference = fopen("log_selector/log_selector_reference.txt", "a");
	//search                 
    _mkdir("log_search");
    log_search_edge		   = fopen("log_search/log_search_edge_reference.txt", "a");
	log_search_err		   = fopen("log_search/log_search_err_reference.txt", "a");
    log_search_bpcount     = fopen("log_search/log_bpcount.txt", "a");
    log_search_bpsad       = fopen("log_search/log_bp_sad.txt", "a");
#else
	log_dummy = fopen("dummy.txt", "w");
#endif
}

int DSCCodec::DSC_Algorithm(int isEncoder, dsc_cfg_t* dsc_cfg, pic_t* ip, pic_t* op, unsigned char* cmpr_buf, pic_t **temp_pic, int *chunk_sizes)
{
	if (isEncoder) {
		printf("this version support only decoding");
		return 0;
	}
	else // decoder
	{

		pic_t *orig_op = NULL;

		int done = 0;

		Init(dsc_cfg, chunk_sizes);

		orig_op = op;

		if (dsc_cfg->convert_rgb) {
			op = temp_pic[1];
		}

		prev_line.Init(dsc_cfg, dsc_cfg->slice_width + PADDING_LEFT + PADDING_RIGHT);
		curr_line.Init(dsc_cfg, BUFFER_FIX);

		// read first group's worth of data
		rc.UpdatePrevNumBIts(vld.numBits);
		if (dsc_cfg->muxing_mode)
			mux.ProcessGroupDec(dsc_cfg, &(postMuxNumBits), cmpr_buf);
		for (int i = 0; i < NUM_COMPONENTS; ++i)
			vld.VLDUnit(dsc_cfg, group_count, wire.masterQp_dec, cpntBitDepth, i, &cmpr_buf, wire.flatness_type,
				mux.shifter, wire.ich_sel, wire.ich_sel_prev, wire.ich_lookup, wire.quantized_residual[i], wire.use_midpoint, wire.rc_size_unit, wire.first_flat, postMuxNumBits);
		vld.BackUpQp(wire.masterQp_dec);
		flatness.DecFlatnessDecision(group_count, wire.first_flat);
		while (!done) {
#ifdef LOG
			//quant_residual
			fprintf(log_front_vld_rate, "\n@%4d:%4d_______________________\n", group_count, group_count_line);
			fprintf(log_front_vld_rate, "*predicted_size: %3d %3d %3d\n", vld.predicted_size[0], vld.predicted_size[1], vld.predicted_size[2]);
			fprintf(log_front_vld_rate, "*rc_unit_size:   %3d %3d %3d  [%3d]\n", wire.rc_size_unit[0], wire.rc_size_unit[1], wire.rc_size_unit[2], vld.numBits - rc.prevNumBits);
			if (wire.ich_sel) {
				fprintf(log_front_vld_rate, "*ich_index: %3d %3d %3d\n", wire.ich_lookup[0], wire.ich_lookup[1], wire.ich_lookup[2]);
			}
			else {
				fprintf(log_front_vld_rate, "*quantized residual [mid]\n");
				fprintf(log_front_vld_rate, "%4d %4d %4d [%d]\n", wire.quantized_residual[0][0], wire.quantized_residual[0][1], wire.quantized_residual[0][2],wire.use_midpoint[0]);
				fprintf(log_front_vld_rate, "%4d %4d %4d [%d]\n", wire.quantized_residual[1][0], wire.quantized_residual[1][1], wire.quantized_residual[1][2], wire.use_midpoint[1]);
				fprintf(log_front_vld_rate, "%4d %4d %4d [%d]\n", wire.quantized_residual[2][0], wire.quantized_residual[2][1], wire.quantized_residual[2][2], wire.use_midpoint[2]);
			}
#endif
			//calculate current group size
			if ((group_count_line + 1) * PIXELS_PER_GROUP >= dsc_cfg->slice_width) {
				group_size = dsc_cfg->slice_width%PIXELS_PER_GROUP;
                if (group_size == 0)
                    group_size = PIXELS_PER_GROUP;
				last_group = true;
			}
			else {
				group_size = PIXELS_PER_GROUP;
				last_group = false;
			}

			// Update IchHistory & look up 
			// Note that UpdateICHistory works on the group to the left // parallel for cpnt
#ifdef LOG
			fprintf(log_ich_sel, "%d\t%d\t%d\t%d\t%d\n",dsc_cfg->slice_width,dsc_cfg->pic_width, group_count_line,(vPos==0),wire.ich_sel_prev);
	#if ORI
			ich.UpdateICHistory_DEC(dsc_cfg->slice_width, dsc_cfg->pic_width, prev_line.line, (vPos == 0), group_count_line, wire.ich_sel_prev, wire.current_CBuff, log_ich_recon);
	#endif
	#if MODI
			ich.UpdateICH_test_DEC(dsc_cfg->slice_width, dsc_cfg->pic_width, prev_line.line, (vPos == 0), group_count_line, wire.ich_sel_prev, wire.current_CBuff, log_ich_recon);
	#endif
#else
	#if ORI
			ich.UpdateICHistory_DEC(dsc_cfg->slice_width, dsc_cfg->pic_width, prev_line.line, (vPos == 0), group_count_line, wire.ich_sel_prev, wire.current_CBuff, log_dummy);
	#endif
	#if MODI
			ich.UpdateICH_test_DEC(dsc_cfg->slice_width, dsc_cfg->pic_width, prev_line.line, (vPos == 0), group_count_line, wire.ich_sel_prev, wire.current_CBuff, log_dummy);
	#endif
#endif


			// IC$ selected on decoder - do an ICH look-up // parallel for pixel & cpnt
#ifndef REVERSE_pixel_ich
			for (int pixel = 0; pixel < group_size; ++pixel) {
#else
			for (int pixel = group_size - 1; pixel >= 0; --pixel) {
#endif // !REVERSE_pixel_ich	
				ich.HistoryLookup(dsc_cfg->slice_width, group_count_line, prev_line.line, wire.ich_lookup[pixel], wire.ich_value[pixel], (vPos == 0));
#ifdef LOG
				fprintf(log_ich_index, "%d\t", wire.ich_lookup[pixel]);
				fprintf(log_ich_value, "%3d %3d %3d ", wire.ich_value[pixel][0], wire.ich_value[pixel][1], wire.ich_value[pixel][2] );
#endif	
			}
#ifdef LOG
			if (group_size != 3)
			{
				if (group_size == 2) {
					fprintf(log_ich_index, "%d\t", 0); 
				}
				else if (group_size == 1) {
					fprintf(log_ich_index, "%d\t%d\t", 0, 0);
				}
				else //0
				{
					fprintf(log_ich_index, "%d\t%d\t%d\t", 0, 0, 0);
				}
			}

			//print log prev line 7 pixel
			int hpos_;
			hpos_ = group_count_line*PIXELS_PER_GROUP + 1;
			hpos_ =CLAMP(hpos_, ICH_PIXELS_ABOVE/2,dsc_cfg->slice_width - 1 - (ICH_PIXELS_ABOVE / 2));
			
			int h_offset_array_idx;
			h_offset_array_idx = group_count_line * 3 + PADDING_LEFT;

			//print pred log prev line 
			fprintf(log_pred_prev, "%3d %3d %3d ", prev_line.line[0][h_offset_array_idx - 2], prev_line.line[1][h_offset_array_idx - 2], prev_line.line[2][h_offset_array_idx - 2]);	//g
			fprintf(log_pred_prev, "%3d %3d %3d ", prev_line.line[0][h_offset_array_idx - 1], prev_line.line[1][h_offset_array_idx - 1], prev_line.line[2][h_offset_array_idx - 1]);	//c
			fprintf(log_pred_prev, "%3d %3d %3d ", prev_line.line[0][h_offset_array_idx], prev_line.line[1][h_offset_array_idx], prev_line.line[2][h_offset_array_idx]);	//b
			fprintf(log_pred_prev, "%3d %3d %3d ", prev_line.line[0][h_offset_array_idx + 1], prev_line.line[1][h_offset_array_idx + 1], prev_line.line[2][h_offset_array_idx + 1]);	//d
			fprintf(log_pred_prev, "%3d %3d %3d ", prev_line.line[0][h_offset_array_idx + 2], prev_line.line[1][h_offset_array_idx + 2], prev_line.line[2][h_offset_array_idx + 2]);	//e
			fprintf(log_pred_prev, "%3d %3d %3d\n", prev_line.line[0][h_offset_array_idx + 3], prev_line.line[1][h_offset_array_idx + 3], prev_line.line[2][h_offset_array_idx + 3]);	//f
			
			fprintf(log_ich_prev_line, "%3d %3d %3d ", prev_line.line[0][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 0], prev_line.line[1][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 0], prev_line.line[2][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 0]); //0
			fprintf(log_ich_prev_line, "%3d %3d %3d ", prev_line.line[0][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 1], prev_line.line[1][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 1], prev_line.line[2][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 1]); //1
			fprintf(log_ich_prev_line, "%3d %3d %3d ", prev_line.line[0][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 2], prev_line.line[1][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 2], prev_line.line[2][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 2]); //2
			fprintf(log_ich_prev_line, "%3d %3d %3d ", prev_line.line[0][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 3], prev_line.line[1][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 3], prev_line.line[2][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 3]); //3
			fprintf(log_ich_prev_line, "%3d %3d %3d ", prev_line.line[0][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 4], prev_line.line[1][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 4], prev_line.line[2][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 4]); //4
			fprintf(log_ich_prev_line, "%3d %3d %3d ", prev_line.line[0][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 5], prev_line.line[1][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 5], prev_line.line[2][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 5]); //5
			fprintf(log_ich_prev_line, "%3d %3d %3d \n", prev_line.line[0][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 6], prev_line.line[1][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 6], prev_line.line[2][hpos_ + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2) + 6]); //6

			fprintf(log_ich_index, "\n");
			fprintf(log_ich_value, "\n");
			// Predict & reconstruction // parallel for pixel & cpnt
			pred.Prediction((vPos == 0), group_count_line, group_size, wire.masterQp_dec, cpntBitDepth, curr_line.line, prev_line.line, wire.use_midpoint, wire.quantized_residual, wire.pred_value, log_pred_pred_val);
#else
			// Predict & reconstruction // parallel for pixel & cpnt
			pred.Prediction((vPos == 0), group_count_line, group_size, wire.masterQp_dec, cpntBitDepth, curr_line.line, prev_line.line, wire.use_midpoint, wire.quantized_residual, wire.pred_value,log_dummy);
#endif
#ifdef LOG
			fprintf(log_pred_signal,"%d %d %d %d %d %d %d %d %d\n",(vPos == 0), group_count_line, wire.masterQp_dec, cpntBitDepth[0], cpntBitDepth[1], cpntBitDepth[2], curr_line.line[0][BUFFER_FIX - PIXELS_PER_GROUP - 1], curr_line.line[1][BUFFER_FIX - PIXELS_PER_GROUP - 1], curr_line.line[2][BUFFER_FIX - PIXELS_PER_GROUP - 1]);
			
			fprintf(log_pred_mid,"%d %d %d %d %d %d %d %d %d\n",wire.use_midpoint[0], wire.use_midpoint[1], wire.use_midpoint[2], wire.use_midpoint[0], wire.use_midpoint[1], wire.use_midpoint[2], wire.use_midpoint[0], wire.use_midpoint[1], wire.use_midpoint[2]);

			fprintf(log_pred_residual, "%4d%4d%4d ",  wire.quantized_residual[0][0], wire.quantized_residual[1][0], wire.quantized_residual[2][0]);
			fprintf(log_pred_residual, "%4d%4d%4d ",  wire.quantized_residual[0][1], wire.quantized_residual[1][1], wire.quantized_residual[2][1]);
			fprintf(log_pred_residual, "%4d%4d%4d\n", wire.quantized_residual[0][2], wire.quantized_residual[1][2], wire.quantized_residual[2][2]);

			fprintf(log_pred_predmode, "%3d \n", (int)pred.prevLinePred[group_count_line]);
			//currline buff
			for (int buff_idx = BUFFER_FIX -1 -PIXELS_PER_GROUP; buff_idx >= 0; buff_idx--)
			{
				fprintf(log_pred_curr, "%3d %3d %3d ", curr_line.line[0][buff_idx], curr_line.line[1][buff_idx], curr_line.line[2][buff_idx]);
			}
			fprintf(log_pred_curr, "\n");

			//prediction reconstruction log
			fprintf(log_pred_reconstruct, "%3d %3d %3d ", wire.pred_value[0][0], wire.pred_value[0][1], wire.pred_value[0][2]);
			fprintf(log_pred_reconstruct, "%3d %3d %3d ", wire.pred_value[1][0], wire.pred_value[1][1], wire.pred_value[1][2]);
			fprintf(log_pred_reconstruct, "%3d %3d %3d \n", wire.pred_value[2][0], wire.pred_value[2][1], wire.pred_value[2][2]);

			// Select right value // parallel for pixel & cpnt
			fprintf(log_pred_ichuse, "%d \n", wire.ich_sel);
			
			//fprintf(log_pred_leftrecon, "%3d %3d %3d\n", curr_line.line[0][BUFFER_FIX - PIXELS_PER_GROUP - 1 - 2], curr_line.line[1][BUFFER_FIX - PIXELS_PER_GROUP - 1 - 2], curr_line.line[2][BUFFER_FIX - PIXELS_PER_GROUP - 1 - 2]);
			fprintf(log_pred_leftrecon, "%3d %3d %3d\n", wire.selected_value[group_size - 1][0], wire.selected_value[group_size - 1][1], wire.selected_value[group_size - 1][2]);
			fprintf(log_selector_ichsel, "%d\n", wire.ich_sel);
#endif
			pred.SelectValue(group_count_line, group_size, wire.ich_sel, wire.ich_value, wire.pred_value, wire.selected_value);
#ifdef LOG
			//selected_value[PIXELS_PER_GROUP][NUM_COMPONENTS]; //< Selector to Current Buffer
			fprintf(log_selector_reference, "%3d %3d %3d ",  wire.selected_value[0][0], wire.selected_value[0][1], wire.selected_value[0][2]);
			fprintf(log_selector_reference, "%3d %3d %3d ",  wire.selected_value[1][0], wire.selected_value[1][1], wire.selected_value[1][2]);
			fprintf(log_selector_reference, "%3d %3d %3d\n", wire.selected_value[2][0], wire.selected_value[2][1], wire.selected_value[2][2]);
#endif
			// Write to Current Buffer // parallel for pixel & cpnt
			curr_line.WirteCBuff(wire.selected_value);
			curr_line.ReadCBuff(wire.current_CBuff);

			// Update Left Reconstruction
			pred.UpdateLeftRecon_DEC(dsc_cfg, wire.selected_value[group_size - 1]);//,log_pred_leftrecon);

			// Update block prediction based on real reconstructed values // parallel for pixel & cpnt
#ifdef LOG
			pred.BlockPredSearch_DEC(cpntBitDepth, dsc_cfg->linebuf_depth, dsc_cfg->block_pred_enable, wire.current_CBuff, group_count_line, group_size,log_search_err,log_search_edge, log_search_bpcount, log_search_bpsad);
#else
			pred.BlockPredSearch_DEC(cpntBitDepth, dsc_cfg->linebuf_depth, dsc_cfg->block_pred_enable, wire.current_CBuff, group_count_line, group_size, log_dummy, log_dummy, log_dummy, log_dummy);
#endif
			//copy current line data to output picture structure
			OutToImageBuffer(dsc_cfg, group_count_line, group_size, op);

			// reduce number of bits per sample in line buffer (replicate pixels in left/right padding)
			prev_line.UpdateByGroup(dsc_cfg, group_count, group_count_line, curr_line.line, cpntBitDepth);

			curr_line.UpdateCBuff(cpntBitDepth);
			curr_line.ReadCBuff(wire.current_CBuff);

			// Update decoder QP per group
			wire.masterQp_dec = flatness.QpAdjustment(dsc_cfg, wire.masterQp_dec, wire.flatness_type, &rc.st_qp);

			rc.RcFunction(dsc_cfg, (vPos == 0), group_count, group_size, wire.rc_size_unit, vld.numBits);

			group_count++;
#ifdef LOG
			fprintf(log_front_vld_rate, "*master_qp [%2d]\n", wire.masterQp_dec);
			fprintf(log_front_vld_rate, "*buffer_fullness: %5d\n", rc.bufferFullness);
			fprintf(log_front_vld_rate, "*scale [%2d] rcXofs [%6d]\n", rc.scale, rc.rcXformOffset);
#endif
			// Don't decode if we're done
			if ((group_count_line*PIXELS_PER_GROUP + group_size - 1 < dsc_cfg->slice_width - 1) || (vPos < dsc_cfg->slice_height - 1)) {
				rc.UpdatePrevNumBIts(vld.numBits);
				if (dsc_cfg->muxing_mode)
					mux.ProcessGroupDec(dsc_cfg, &(postMuxNumBits), cmpr_buf);
				for (int i = 0; i<NUM_COMPONENTS; ++i)
					vld.VLDUnit(dsc_cfg, group_count, wire.masterQp_dec, cpntBitDepth, i, &cmpr_buf, wire.flatness_type, 
						mux.shifter, wire.ich_sel, wire.ich_sel_prev, wire.ich_lookup, wire.quantized_residual[i], wire.use_midpoint, wire.rc_size_unit, wire.first_flat, postMuxNumBits);
				vld.BackUpQp(wire.masterQp_dec);
				flatness.DecFlatnessDecision(group_count, wire.first_flat);
			}
			group_count_line++;
			if (group_count_line*PIXELS_PER_GROUP >= dsc_cfg->slice_width) {
				group_count_line = 0;
				vPos++;
				if (vPos >= dsc_cfg->slice_height) {
					done = 1;
				}
			}
		}

		if (dsc_cfg->convert_rgb) {
			// Convert YCoCg back to RGB again
			cvt.ycocg2rgb(op, orig_op, dsc_cfg->xstart, dsc_cfg->ystart, dsc_cfg->slice_height, dsc_cfg->slice_width);
		}

#ifdef LOG
		fclose(log_front_vld_rate);

		fclose(log_pred_signal);
		fclose(log_pred_mid);
		fclose(log_pred_residual);
		fclose(log_pred_prev);
		fclose(log_pred_curr);
		fclose(log_pred_predmode);
		fclose(log_pred_reconstruct);
		fclose(log_pred_leftrecon);
		fclose(log_pred_ichuse);
		fclose(log_pred_pred_val);

        fclose(log_ich_index);
        fclose(log_ich_value);
        fclose(log_ich_sel);
        fclose(log_ich_recon);
        fclose(log_ich_prev_line);

        fclose(log_selector_ichsel);
        fclose(log_selector_reference);

		fclose(log_search_edge);
		fclose(log_search_err);
		fclose(log_search_bpcount);
        fclose(log_search_bpsad);

#else
		fclose(log_dummy);
#endif
		return postMuxNumBits;
	}
}

void DSCCodec::OutToImageBuffer(dsc_cfg_t* dsc_cfg, int group_num, int group_size, pic_t* op)
{
#ifndef REVERSE
	for (int pixel = 0; pixel < group_size; ++pixel) {
#else
	for (int pixel = group_size - 1; pixel >= 0; --pixel) {
#endif // !REVERSE	
#ifndef REVERSE
		for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
#else
		for (int cpnt = NUM_COMPONENTS - 1; cpnt >= 0; cpnt--) {
#endif // !REVERSE	
			if ((vPos + dsc_cfg->ystart < op->h) && (PIXELS_PER_GROUP*group_count_line + pixel + dsc_cfg->xstart < op->w)) {
				switch (cpnt) {
				case 0: op->data.yuv.y[vPos + dsc_cfg->ystart][PIXELS_PER_GROUP*group_count_line + pixel + dsc_cfg->xstart] = curr_line.line[cpnt][BUFFER_FIX - PIXELS_PER_GROUP + pixel]; break;
				case 1: op->data.yuv.u[vPos + dsc_cfg->ystart][PIXELS_PER_GROUP*group_count_line + pixel + dsc_cfg->xstart] = curr_line.line[cpnt][BUFFER_FIX - PIXELS_PER_GROUP + pixel]; break;
				case 2: op->data.yuv.v[vPos + dsc_cfg->ystart][PIXELS_PER_GROUP*group_count_line + pixel + dsc_cfg->xstart] = curr_line.line[cpnt][BUFFER_FIX - PIXELS_PER_GROUP + pixel]; break;
				}
			}
		}
	}
}
