#ifndef _DSC_TYPES_H_
#define _DSC_TYPES_H_

#include "fifo.h"

//#define REVERSE_pixel_ich
#define REVERSE_pixel_pred
//#define REVERSE_cpnt_pred
#define REVERSE_pixel_sel
//#define REVERSE_cpnt_sel
//#define REVERSE_pixel_bps
//#define REVERSE_cpnt_bps
//#define REVERSE


#define NUM_BUF_RANGES        15
#define NUM_COMPONENTS        3
#define MAX_UNITS_PER_GROUP   3
#define SAMPLES_PER_UNIT      3
#define PIXELS_PER_GROUP	  3
#define GROUPS_PER_SUPERGROUP 4
#define BP_RANGE              10
#define BP_SIZE				  3
#define PRED_BLK_SIZE		  3
#define ICH_BITS			  5
#define ICH_SIZE			  (1<<ICH_BITS)
#define ICH_PIXELS_ABOVE      7
#define OFFSET_FRACTIONAL_BITS  11
#define MAX_SE_SIZE           (4*dsc_cfg->bits_per_component+4)
#define PPS_SIZE			  128
#define BP_EDGE_COUNT		  3
#define BP_EDGE_STRENGTH      32
#define BUFFER_FIX			  13
#define PADDING_LEFT          5  // Pixels to pad line arrays to the left
#define PADDING_RIGHT         5  // Pixels to pad line arrays to the right
#define RC_SCALE_BINARY_POINT   3
#define SOMEWHAT_FLAT_QP_THRESH (7+(2*(dsc_cfg->bits_per_component-8)))
#define OVERFLOW_AVOID_THRESHOLD  (-172)

typedef enum { PT_MAP=0, PT_LEFT, PT_BLOCK } PRED_TYPE;

#define NUM_PRED_TYPES        (PT_BLOCK+BP_RANGE)

// Configuration for a single RC model range
typedef struct dsc_range_cfg_s {
	int  range_min_qp;			//< Min QP allowed for this range
	int  range_max_qp;			//< Max QP allowed for this range
	int  range_bpg_offset;		//< Bits/group offset to apply to target for this group
} dsc_range_cfg_t;

// Codec configuration
typedef struct dsc_cfg_s {
	int  linebuf_depth;		//< Bits / component for previous reconstructed line buffer
	int  rcb_bits;			//< Rate control buffer size (in bits); not in PPS, used only in C model for checking overflow
	int  bits_per_component; //< Bits / component to code (must be 8, 10, or 12)
	int  convert_rgb;		//< Flag indicating to do RGB - YCoCg conversion and back (should be 1 for RGB input)
	int  slice_width;		//< Slice width
	int  slice_height;		//< Slice height
	int  enable_422;        //< 4:2:2 enable mode (from PPS, 4:2:2 conversion happens outside of DSC encode/decode algorithm)
	int  pic_width;			//< Picture width
	int  pic_height;		//< Picture height
	int  rc_tgt_offset_hi;		//< Offset to bits/group used by RC to determine QP adjustment
	int  rc_tgt_offset_lo;		//< Offset to bits/group used by RC to determine QP adjustment
	int  bits_per_pixel;	//< Bits/pixel target << 4 (ie., 4 fractional bits)
	int  rc_edge_factor;	//< Factor to determine if an edge is present based on the bits produced
	int  rc_quant_incr_limit1; //< Slow down incrementing once the range reaches this value
	int  rc_quant_incr_limit0; //< Slow down incrementing once the range reaches this value
	int  initial_xmit_delay;	//< Number of pixels to delay the initial transmission
	int  initial_dec_delay;		//< Number of pixels to delay the VLD on the decoder, not including SSM
	int  block_pred_enable;	//< Block prediction range (in pixels)
	int  first_line_bpg_ofs; //< Bits/group offset to use for first line of the slice
	int  initial_offset;    //< Value to use for RC model offset at slice start
	int  xstart;			//< X position in the picture of top-left corner of slice
	int  ystart;			//< Y position in the picture of top-left corner of slice
	int  rc_buf_thresh[NUM_BUF_RANGES-1];   //< Thresholds defining each of the buffer ranges
	dsc_range_cfg_t rc_range_parameters[NUM_BUF_RANGES];  //< Parameters for each of the RC ranges
	int  rc_model_size;     //< Total size of RC model
	int  flatness_min_qp;	//< Minimum QP where flatness information is sent
	int  flatness_max_qp;   //< Maximum QP where flatness information is sent
	int  flatness_det_thresh;  //< MAX-MIN for all components is required to be <= this value for flatness to be used
	int  initial_scale_value;  //< Initial value for scale factor
	int  scale_decrement_interval;   //< Decrement scale factor every scale_decrement_interval groups
	int  scale_increment_interval;   //< Increment scale factor every scale_increment_interval groups
	int  nfl_bpg_offset;		//< Non-first line BPG offset to use
	int  slice_bpg_offset;		//< BPG offset used to enforce slice bit constraint
	int  final_offset;          //< Final RC linear transformation offset value
	int  vbr_enable;			//< Enable on-off VBR (ie., disable stuffing bits)
	int  muxing_mode;           //< 0 = standard DSU, 1 = substream muxing, 2 = concatenated (not yet supported)
	int  mux_word_size;         //< Mux word size (in bits) for SSM mode
	int  chunk_size;            //< The (max) size in bytes of the "chunks" that are used in slice multiplexing
	int  pps_identifier;		//< Placeholder for PPS identifier
} dsc_cfg_t;

typedef struct wire_s
{
	int masterQp_dec; //< QP used for the current group

	int rc_size_unit[MAX_UNITS_PER_GROUP];  //< VLD to RC < Size for each unit assuming size was perfectly predicted
	int first_flat; //< VLD to Flat < If -1, none of the 4 group set are flat, otherwise indicates which group is the one where flatness starts
	int flatness_type; //< VLD to Flat < 0 = somewhat flat; 1 = very flat

	int quantized_residual[MAX_UNITS_PER_GROUP][SAMPLES_PER_UNIT]; //< VLD to Pred < quantized residual for current group
	int use_midpoint[MAX_UNITS_PER_GROUP]; //< VLD to Pred < Decoder flags indicating for each unit whether midpoint prediction should be used 

	int ich_sel;	//< VLD to Selector < Flag indicating ICH is used for current group

	int ich_sel_prev;	//< VLD to ICH <Flag indicating ICH is used for previous group
	int ich_lookup[PIXELS_PER_GROUP]; //< VLD to ICH < ICH indices for current group

	int pred_value[PIXELS_PER_GROUP][NUM_COMPONENTS]; //< Pred to Selector
	unsigned int ich_value[PIXELS_PER_GROUP][NUM_COMPONENTS]; //< ICH to Selector

	int selected_value[PIXELS_PER_GROUP][NUM_COMPONENTS]; //< Selector to Current Buffer
	int current_CBuff[NUM_COMPONENTS][BUFFER_FIX]; //< Current Buffer to Predictor/Previous Buffer
} wire_t;

#endif // __DSC_TYPES_H_
