#ifndef _PSNR_H
#define _PSNR_H

#include "vdo.h"

void compute_and_display_PSNR(pic_t *p_in, pic_t *p_out, int bpp, FILE *logfp);
#endif
