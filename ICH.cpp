 #include "ICH.h"

void ICH::Init(dsc_cfg_t *dsc_cfg)
{
	InitQuantTable(dsc_cfg);
	history.valid = new int[ICH_SIZE];
	for (int i = 0; i < NUM_COMPONENTS; i++) {
		history.pixels[i] = new unsigned int[ICH_SIZE];
	}
	for (int j = 0; j < ICH_SIZE; j++) {
		history.valid[j] = 0;
	}
}
#if MODI
/*
int ICH::HitMissDetect(unsigned int p[NUM_COMPONENTS][PIXELS_PER_GROUP],int id,int first_line_flag){
    int reserved;
    int hit[3] = { 0,0,0};
    reserved = first_line_flag ? ICH_SIZE : (ICH_SIZE-ICH_PIXELS_ABOVE);
    for(int pixel = 0; pixel < id; ++pixel){
        for(int i = 0; i < reserved; ++i){
            if(!history.valid[i]){
                hit[pixel] = 0;
                break;
            }
            if( (p[0][pixel] == history.pixels[0][i]) && 
                (p[1][pixel] == history.pixels[1][i]) && 
                (p[2][pixel] == history.pixels[2][i]) ){
                    hit[pixel] = 1;
                    break;
            }
        }
    }
    if( (hit[0] == 1) || 
        (hit[1] == 1) || 
        (hit[2] == 1) )
        return 1; // hit
    else
        return 0;
}
*/
void ICH::UpdateICH_test_DEC (int slice_width, int pic_width, int **prevLine, int first_line_flag, int group_num, int prevIchSelected, int currLine[NUM_COMPONENTS][BUFFER_FIX],FILE *log)
{
    int i;
    int cpnt;
    int mode = 0;
    //unsigned int p[NUM_COMPONENTS][PIXELS_PER_GROUP];   //PIXEL_PIXELPERGROUP
    //unsigned int *p[NUM_COMPONENTS];   //PIXEL_PIXELPERGROUP
    unsigned int  p[NUM_COMPONENTS][PIXELS_PER_GROUP];
    unsigned int  p_ps[NUM_COMPONENTS][PIXELS_PER_GROUP];

    //for(int i = 0; i < NUM_COMPONENTS; ++i)
    //    p[i] = new unsigned int[PIXELS_PER_GROUP];
    //Nothing to do if ich disabled
    if(ICH_BITS == 0)
        return;
    // Reset ICH at beginning of each line if multiple slices per line
    if(group_num == 0){
        //beginning of slice
        if(first_line_flag){
            for(i = 0; i < ICH_SIZE; i++)
                history.valid[i] = 0;
        }
        //multiple slice per line
        else if(slice_width != pic_width){
            for(i = 0; i < (ICH_SIZE - ICH_PIXELS_ABOVE); i++)
                history.valid[i] = 0;
        }
        //(log,"%d %d %d %d %d %d %d %d %d \n", 0,0,0,0,0,0,0,0,0);
        // doesn't update ICH
        return;
    }
	for (int pixel = 0; pixel < PIXELS_PER_GROUP;++pixel) {
        for(cpnt = 0; cpnt < NUM_COMPONENTS; ++cpnt){
            p[cpnt][pixel] = currLine[cpnt][BUFFER_FIX - 2 * PIXELS_PER_GROUP + pixel];
            fprintf(log, "%d ", p[cpnt][pixel]);
        }
    }
    //compression
    if( (p[0][0] == p[0][1] && p[1][0] == p[1][1] && p[2][0] == p[2][1]) &&
        (p[0][0] == p[0][2] && p[1][0] == p[1][2] && p[2][0] == p[2][2]) ){ //a a a
        history.onlyone = 1;
        mode = 0;
        p_ps[0][0] = p[0][0];
        p_ps[1][0] = p[1][0];
        p_ps[2][0] = p[2][0];
        p_ps[0][1] = p[0][1];
        p_ps[1][1] = p[1][1];
        p_ps[2][1] = p[2][1];
        p_ps[0][2] = p[0][2];
        p_ps[1][2] = p[1][2];
        p_ps[2][2] = p[2][2];
    }
    else if( (p[0][0] == p[0][1] && p[1][0] == p[1][1] && p[2][0] == p[2][1]) &&
             (p[0][0] != p[0][2] || p[1][0] != p[1][2] || p[2][0] != p[2][2])    ){   // a a b
        history.onlyone = 2;
        mode = 1;
		if (prevIchSelected) {
			p_ps[0][0] = p[0][1];
			p_ps[1][0] = p[1][1];
			p_ps[2][0] = p[2][1];
			p_ps[0][1] = p[0][2];
			p_ps[1][1] = p[1][2];
			p_ps[2][1] = p[2][2];
			p_ps[0][2] = p[0][2];
			p_ps[1][2] = p[1][2];
			p_ps[2][2] = p[2][2];
		}
		else {
			p_ps[0][0] = p[0][0];
			p_ps[1][0] = p[1][0];
			p_ps[2][0] = p[2][0];
			p_ps[0][1] = p[0][1];
			p_ps[1][1] = p[1][1];
			p_ps[2][1] = p[2][1];
			p_ps[0][2] = p[0][2];
			p_ps[1][2] = p[1][2];
			p_ps[2][2] = p[2][2];
		}
    }
    else if( (p[0][1] == p[0][2] && p[1][1] == p[1][2] && p[2][1] == p[2][2]) &&
             (p[0][0] != p[0][1] || p[1][0] != p[1][1] || p[2][0] != p[2][1]) ){   // a b b
		history.onlyone = 2;
        mode = 2;
        p_ps[0][0] = p[0][0];
        p_ps[1][0] = p[1][0];
        p_ps[2][0] = p[2][0];
        p_ps[0][1] = p[0][1];
        p_ps[1][1] = p[1][1];
        p_ps[2][1] = p[2][1];
        p_ps[0][2] = p[0][2];
        p_ps[1][2] = p[1][2];
        p_ps[2][2] = p[2][2];
    }
    else if( (p[0][0] == p[0][2] && p[1][0] == p[1][2] && p[2][0] == p[2][2]) &&
             (p[0][0] != p[0][1] || p[1][0] != p[1][1] || p[2][0] != p[2][1]) ){   // a b a
		history.onlyone = 2;
        mode = 3;		
        if(prevIchSelected){        //ich mode            
			p_ps[0][0] = p[0][1];
			p_ps[1][0] = p[1][1];
			p_ps[2][0] = p[2][1];
			p_ps[0][1] = p[0][2];
			p_ps[1][1] = p[1][2];
			p_ps[2][1] = p[2][2];
			p_ps[0][2] = p[0][2];
			p_ps[1][2] = p[1][2];
			p_ps[2][2] = p[2][2];
        }
        else {                  // not ich
        p_ps[0][0] = p[0][0];
        p_ps[1][0] = p[1][0];
        p_ps[2][0] = p[2][0];
        p_ps[0][1] = p[0][1];
        p_ps[1][1] = p[1][1];
        p_ps[2][1] = p[2][1];
        p_ps[0][2] = p[0][2];
        p_ps[1][2] = p[1][2];
        p_ps[2][2] = p[2][2];
        }
    }
    else {                          //a b c
		history.onlyone = 3;
        mode = 4;
        p_ps[0][0] = p[0][0];
        p_ps[1][0] = p[1][0];
        p_ps[2][0] = p[2][0];
        p_ps[0][1] = p[0][1];
        p_ps[1][1] = p[1][1];
        p_ps[2][1] = p[2][1];
        p_ps[0][2] = p[0][2];
        p_ps[1][2] = p[1][2];
        p_ps[2][2] = p[2][2];
    }
    fprintf(log, "\n");
    for(int i = 0; i < ICH_SIZE; ++i){
        history.vector[i] = 0;
    }
    //Update ICH algorithm
    UpdateHistory_test(slice_width, first_line_flag, group_num, history.onlyone,history.valid, prevIchSelected, history.pixels, prevLine, p_ps,mode);
}
void ICH::UpdateHistory_test(int slice_width, int first_line_flag, int group_num, int onlyone,int *valid, int prevIchSelected, unsigned int **pixels, int **prevLine, unsigned int recon[NUM_COMPONENTS][PIXELS_PER_GROUP], int mode)
{
    int reserved;
    int hit;
    int loc = 0;
    int shift[3][32];
    int hitCounter = 0;
	int hit_counter[3] = { 0,0,0 };

    //int miss;
    reserved = first_line_flag ? ICH_SIZE : (ICH_SIZE-ICH_PIXELS_ABOVE);
    // check hit loc
	for (int pixel = 0; pixel < PIXELS_PER_GROUP; ++pixel){ //initialize
		history.hit_miss[pixel] = 0;
        history.hit_loc[pixel] = 9999;
    }
    for(int pixel = 0; pixel < onlyone; ++pixel){
        loc = 999;
        for(int i = 0; i < reserved; ++i){
            hit = 0;
            if(!history.valid[i]){
                loc = i;
                break;
            }
            if( (recon[0][pixel] == pixels[0][i]) && 
                (recon[1][pixel] == pixels[1][i]) && 
                (recon[2][pixel] == pixels[2][i]) ){
                hit = 1;
                loc = i;
                break;
            }
        }
        history.hit_loc[pixel] = loc;
        history.hit_miss[pixel] = hit;
    }
	int temp = 0;
	for (int i = 0; i < onlyone; ++i) {
		if (history.hit_miss[i]) {	//Hit
			for (int j = i + 1; j < onlyone; ++j) {
				if (history.hit_miss[j]) {	//Hit
					if (history.hit_loc[i] >= history.hit_loc[j])
					{
						temp = history.hit_loc[i];
						history.hit_loc[i] = history.hit_loc[j];
						history.hit_loc[j] = temp;
					}
				}
			}
		}
	}

	hitCounter = 0;
	for (int i = 0; i < onlyone; ++i) {
		if (history.hit_miss[i]) {	//Hit
			++hitCounter;
			hit_counter[i] = hitCounter;
		}
		else //Miss
			hit_counter[i] = 0;
	}
	/*
	if (prevIchSelected) {
		//compress hit data
		if (mode == 0) {	// a a a
			history.hit_miss[0] = history.hit_miss[0];
			history.hit_miss[1] = history.hit_miss[1];
			history.hit_miss[2] = history.hit_miss[2];
			history.hit_loc[0] = history.hit_loc[0];
			history.hit_loc[1] = history.hit_loc[1];
			history.hit_loc[2] = history.hit_loc[2];
		}
		else if (mode == 1) {	// a a b
			history.hit_miss[0] = history.hit_miss[0];
			history.hit_miss[1] = history.hit_miss[1];
			history.hit_miss[2] = history.hit_miss[2];
			history.hit_loc[0] = history.hit_loc[0];
			history.hit_loc[1] = history.hit_loc[1];
			history.hit_loc[2] = history.hit_loc[2];
		}
		else if (mode == 2) {	//a b b 
			history.hit_miss[0] = history.hit_miss[0];
			history.hit_miss[1] = history.hit_miss[1];
			history.hit_miss[2] = history.hit_miss[2];
			history.hit_loc[0] = history.hit_loc[0];
			history.hit_loc[1] = history.hit_loc[1];
			history.hit_loc[2] = history.hit_loc[2];
		}
		else if (mode == 3) {	//a b a 
			history.hit_miss[0] = history.hit_miss[0];
			history.hit_miss[1] = history.hit_miss[1];
			history.hit_miss[2] = history.hit_miss[2];
			history.hit_loc[0] = history.hit_loc[0];
			history.hit_loc[1] = history.hit_loc[1];
			history.hit_loc[2] = history.hit_loc[2];
		}
		else {					//a b c
			history.hit_miss[0] = history.hit_miss[0];
			history.hit_miss[1] = history.hit_miss[1];
			history.hit_miss[2] = history.hit_miss[2];
			history.hit_loc[0] = history.hit_loc[0];
			history.hit_loc[1] = history.hit_loc[1];
			history.hit_loc[2] = history.hit_loc[2];
		}
	}
	*/
    //make vector
    #if REAL
    if(!prevIchSelected){   //not ich
        for(int i = 0; i < reserved; ++i)
            history.vector[i] = 3;
    }
    else{
        for(int pixel = 0; pixel < onlyone; ++pixel){
            if(!history.hit_miss[pixel]){               //miss
                for(int i = 0; i < reserved; ++i)
                    history.vector[i] = history.vector[i] + 1;
            }
            else{                               //hit
                for(int i = 0; i <= history.hit_loc[pixel]; ++i)
                    history.vector[i] = history.vector[i] + 1;
            }
        }
    }
    #endif 
    #if UPDATE
    //make vector New algorithm
    for(int location = 0; location < ICH_SIZE; ++location){  
        history.vector[location] = 0;
        shift[0][location] = 0;
        shift[1][location] = 0;
        shift[2][location] = 0;
    }
    if(prevIchSelected){
        for(int i = 0; i < onlyone; ++i){
            if(!history.hit_miss[i]){   //Miss
                for(int location = 0; location < reserved; ++location)
                    shift[i][location] = 1;
            }
            else { //Hit
                for(int location = 0; location <= history.hit_loc[i] + onlyone-hit_counter[i];++location)
                    shift[i][location] = 1;
            }
        }
    }
    else {
        for(int i =0; i < 3; ++i){
            for(int location = 0; location < reserved; ++location){
                shift[i][location] = 1;
            }
        }
    }
    for(int location = 0; location < ICH_SIZE; ++location){
		if (prevIchSelected) {	//Hit
			for (int i = 0; i < onlyone; ++i)
			{
				history.vector[location] += shift[i][location];
			}
		}
		else {	//Miss
			for (int i = 0; i < 3; ++i)
			{
				history.vector[location] += shift[i][location];
			}
		}
    }
    #endif

    //padding recon + history
    for(int cpnt = 0; cpnt < NUM_COMPONENTS; ++cpnt){
        for(int i = 0; i < 3; ++i){
            history.padding[cpnt][i] = recon[cpnt][2-i];
            history.valid_padding[i] = 1;
        }
        for(int i = 3; i < ICH_SIZE+3; ++i)
        {
            history.padding[cpnt][i] = pixels[cpnt][i-3];
			history.valid_padding[i] = history.valid[i - 3];
        }
    }

    #if UPDATE
    for(int i = reserved-1; i > 0; --i){
        TakeHistory_rv1(i,history.vector[i],pixels,history.padding);
    }
    TakeHistory_rv1(0,history.vector[0],pixels,history.padding);
    #endif
    #if REAL
    //Update
    for(int i = reserved-1; i > 0; --i)
    {
        TakeHistory(i,history.vector[i],0,onlyone,pixels,history.padding,prevIchSelected);
    }
        TakeHistory(0,history.vector[0],0,onlyone,pixels,history.padding,prevIchSelected);
    #endif
}
void ICH::TakeHistory_rv1(int location, int vector,unsigned int **pixels,unsigned int padding[NUM_COMPONENTS][ICH_SIZE+3]){
    pixels[0][location] = padding[0][location + 3 - vector];
    pixels[1][location] = padding[1][location + 3 - vector];
    pixels[2][location] = padding[2][location + 3 - vector];
	history.valid[location] = history.valid_padding[location + 3 - vector];
}
void ICH::TakeHistory(int location, int vector, int n,int onlyone, unsigned int **pixels, unsigned int padding[NUM_COMPONENTS][ICH_SIZE+3], int prevIchSelected)
{
    int hitNum = 0;
    int check;
    //chekc # of hit
    for(int i = 0; i < onlyone; ++i){
        if(history.hit_miss[i]){
        if(history.hit_loc[i] < location && history.hit_loc[i] >= location - vector)
            ++hitNum;
        }
    }
    if(!prevIchSelected){
        pixels[0][location]     = padding[0][location + 3 - vector];
        pixels[1][location]     = padding[1][location + 3 - vector];
        pixels[2][location]     = padding[2][location + 3 - vector];
        history.valid[location] = history.valid_padding[location + 3 - vector];
    }
    else{
        if(!hitNum){
            pixels[0][location] = padding[0][location + 3 - vector];
            pixels[1][location] = padding[1][location + 3 - vector];
            pixels[2][location] = padding[2][location + 3 - vector];
            history.valid[location] = history.valid_padding[location +3 - vector];
        }
        else{
            check = 1;
            for(int i = 0; i < onlyone; ++i){
                if( (location - vector - 1) == history.hit_loc[i]){ //check next
                    check = 0;
                    break;
                }
            }
            if(!check){
                pixels[0][location] = padding[0][location + 3 - vector - hitNum - 1];
                pixels[1][location] = padding[1][location + 3 - vector - hitNum - 1];
                pixels[2][location] = padding[2][location + 3 - vector - hitNum - 1];
                history.valid[location] = history.valid_padding[location +3 - vector - hitNum - 1];
            }
            else{
                pixels[0][location] = padding[0][location + 3 - vector - hitNum];
                pixels[1][location] = padding[1][location + 3 - vector - hitNum];
                pixels[2][location] = padding[2][location + 3 - vector - hitNum];
                history.valid[location] = history.valid_padding[location +3 - vector - hitNum];
            }
        }
    }
}
#endif

void ICH::UpdateICHistory_DEC(int slice_width, int pic_width, int **prevLine, int first_line_flag, int group_num, int prevIchSelected, int currLine[NUM_COMPONENTS][BUFFER_FIX],FILE* log)
{
	int i;
	int cpnt;
	unsigned int p[NUM_COMPONENTS];

	// Nothing to do if ICH disabled
	if (ICH_BITS == 0)
		return;			

	// Reset ICH at beginning of each line if multiple slices per line
	if (group_num == 0) {
		// Beginning of slice
		if (first_line_flag) {
			for (i = 0; i<ICH_SIZE; ++i)
				history.valid[i] = 0;
		}
		// Multiple slices per line
		else if (slice_width != pic_width) {
			for (i = 0; i<(ICH_SIZE - ICH_PIXELS_ABOVE); ++i)
				history.valid[i] = 0;
		}
		fprintf(log, "%d %d %d %d %d %d %d %d %d \n", 0,0,0,0,0,0,0,0,0);
		// Current code doesn't update ICH for last group of each line -- probably not much correlation anyway
		return;
	}

	for (int pixel = 0; pixel < PIXELS_PER_GROUP; ++pixel) {
		// Get final reconstructed pixel value
		for (cpnt = 0; cpnt < NUM_COMPONENTS; ++cpnt) {
			p[cpnt] = currLine[cpnt][BUFFER_FIX - 2 * PIXELS_PER_GROUP + pixel];
			fprintf(log, "%d ", p[cpnt]);
		}
		
		// Update ICH accordingly
		UpdateHistoryElement(slice_width, first_line_flag, group_num, history.valid, prevIchSelected, history.pixels, prevLine, p);
	}
	fprintf(log, "\n");
}

void ICH::HistoryLookup(int slice_width, int group_num, int **prevLine, int entry, unsigned int *p, int first_line_flag)
{
	int reserved;
	int hpos_;

	reserved = ICH_SIZE - ICH_PIXELS_ABOVE;

	if (!first_line_flag && (entry >= reserved)) {
		// Center pixel of group as reference
		hpos_ = group_num*PIXELS_PER_GROUP + 1;
		// Keeps upper line history entries unique at left & right edge
		hpos_ = CLAMP(hpos_, ICH_PIXELS_ABOVE / 2, slice_width - 1 - (ICH_PIXELS_ABOVE / 2));  
		p[0] = prevLine[0][hpos_ + (entry - reserved) + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2)];
		p[1] = prevLine[1][hpos_ + (entry - reserved) + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2)];
		p[2] = prevLine[2][hpos_ + (entry - reserved) + PADDING_LEFT - (ICH_PIXELS_ABOVE / 2)];
	}
	else {
		p[0] = history.pixels[0][entry];
		p[1] = history.pixels[1][entry];
		p[2] = history.pixels[2][entry];
	}
}

void ICH::UpdateHistoryElement(int slice_width, int first_line_flag, int group_num, int *valid, int prevIchSelected, unsigned int **pixels, int **prevLine, unsigned int *recon)    //renew
{
	int hit, j;
	int loc;
	unsigned int ich_pixel[NUM_COMPONENTS];
	int reserved;

	// *MODEL NOTE* MN_ICH_UPDATE
	reserved = first_line_flag ? ICH_SIZE : (ICH_SIZE - ICH_PIXELS_ABOVE);  // space for UL, U, UR

	// Update the ICH with recon as the MRU
	hit = 0;
	// If no match, delete LRU
	loc = reserved - 1; 
	for (j = 0; j<reserved; ++j) {
		// Can replace any empty entry
		if (!valid[j]) {
			loc = j;
			break;
		}
		// Specific hPos within group is not critical since hits against UL/U/UR don't have specific detection
		HistoryLookup(slice_width, group_num, prevLine, j, ich_pixel, first_line_flag);
		hit = 1;

		if (ich_pixel[0] != recon[0])
			hit = 0;
		if (ich_pixel[1] != recon[1])
			hit = 0;
		if (ich_pixel[2] != recon[2])
			hit = 0;

		if (hit && prevIchSelected) {
			loc = j;
			break;  // Found one
		}
	}

	int *temp;
	unsigned int **temp_pixels;

	temp = valid;
	temp_pixels = pixels;
	for (j = loc; j > 0; --j) {
		valid[j] = temp[j - 1];
		// Delete from current position (or delete LRU)                        
		pixels[0][j] = temp_pixels[0][j - 1];
		pixels[1][j] = temp_pixels[1][j - 1];
		pixels[2][j] = temp_pixels[2][j - 1];
	}

	// Insert as most recent
	pixels[0][0] = recon[0];
	pixels[1][0] = recon[1];
	pixels[2][0] = recon[2];
	valid[0] = 1;
}

