#include "cvt.h"

void CVT::Init()
{
}

void CVT::rgb2ycocg(pic_t *ip, pic_t *op, int xstart, int ystart, int slice_height,int slice_width)
{
	int i, j;
	int r, g, b;
	int y, co, cg;
	int half;

	if (ip->chroma != YUV_444)
	{
		fprintf(stderr, "ERROR: rgb2yuv() Incorrect input chroma type.\n");
		exit(1);
	}

	if (ip->color != RGB)
	{
		fprintf(stderr, "ERROR: rgb2yuv() Incorrect input color type.\n");
		exit(1);
	}

	if (ip->w != op->w || ip->h != op->h)
	{
		fprintf(stderr, "ERROR: rgb2yuv() Incorrect picture size.\n");
		exit(1);
	}

	if (op->chroma != YUV_444)
	{
		fprintf(stderr, "ERROR: rgb2yuv() Incorrect output chroma type.\n");
		exit(1);
	}

	if (op->color != YUV_SD && op->color != YUV_HD)
	{
		fprintf(stderr, "ERROR: rgb2yuv() Incorrect output color type.\n");
		exit(1);
	}

	switch (ip->bits) {
	case 8:
		half = 128;
		break;
	case 10:
		half = 512;
		break;
	case 12:
		half = 2048;
		break;
	default:
		fprintf(stderr, "ERROR: rgb2yuv() Unsupported bit resolution (bits=%d).\n", ip->bits);
		exit(1);
		break;
	}


	for (i = ystart; i < ystart + slice_height; i++)
	{
		if (i >= ip->h)
			break;
		for (j = xstart; j < xstart + slice_width; j++)
		{
			int t;
			if (j >= ip->w)
				break;
			r = ip->data.rgb.r[i][j];
			g = ip->data.rgb.g[i][j];
			b = ip->data.rgb.b[i][j];

			// *MODEL NOTE* MN_ENC_CSC
			co = r - b;
			t = b + (co >> 1);
			cg = g - t;
			y = t + (cg >> 1);

			op->data.yuv.y[i][j] = y;
#ifdef REDUCE_CHROMA_12BPC
			if (ip->bits == 12)
			{
				op->data.yuv.u[i][j] = ((co + 1) >> 1) + half;
				op->data.yuv.v[i][j] = ((cg + 1) >> 1) + half;
			}
			else
#endif
			{
				op->data.yuv.u[i][j] = co + half * 2;
				op->data.yuv.v[i][j] = cg + half * 2;
			}
		}
	}
}

void CVT::ycocg2rgb(pic_t *ip, pic_t *op, int xstart, int ystart, int slice_height, int slice_width)
{
	int i, j;
	int y, co, cg, r, g, b;
	int half, max;

	if (ip->chroma != YUV_444)
	{
		fprintf(stderr, "ERROR: Incorrect input chroma type.\n");
		exit(1);
	}

	if (ip->color != YUV_SD && ip->color != YUV_HD)
	{
		fprintf(stderr, "ERROR: Incorrect input color type.\n");
		exit(1);
	}

	if (ip->w != op->w || ip->h != op->h)
	{
		fprintf(stderr, "ERROR: Incorrect picture size.\n");
		exit(1);
	}

	if (op->chroma != YUV_444)
	{
		fprintf(stderr, "ERROR: Incorrect output chroma type.\n");
		exit(1);
	}

	if (op->color != RGB)
	{
		fprintf(stderr, "ERROR: Incorrect output color type.\n");
		exit(1);
	}


	switch (ip->bits) {
	case 8:
		half = 128;
		max = 255;
		break;
	case 10:
		half = 512;
		max = 1023;
		break;
	case 12:
		half = 2048;
		max = 4095;
		break;
	default:
		fprintf(stderr, "ERROR: Unsupported bit resolution (bits=%d).\n", ip->bits);
		exit(1);
		break;
	}

	for (i = ystart; i < ystart + slice_height; i++)
	{
		if (i >= ip->h)
			break;
		for (j = xstart; j < xstart + slice_width; j++)
		{
			int t;

			if (j >= ip->w)
				break;

			// *MODEL NOTE* MN_DEC_CSC
			y = ip->data.yuv.y[i][j];
#ifdef REDUCE_CHROMA_12BPC
			if (ip->bits == 12)
			{
				co = (ip->data.yuv.u[i][j] - half) << 1;
				cg = (ip->data.yuv.v[i][j] - half) << 1;
			}
			else
#endif
			{
				co = ip->data.yuv.u[i][j] - half * 2;
				cg = ip->data.yuv.v[i][j] - half * 2;
			}

			t = y - (cg >> 1);
			g = cg + t;
			b = t - (co >> 1);
			r = co + b;

			op->data.rgb.r[i][j] = CLAMP(r, 0, max);
			op->data.rgb.g[i][j] = CLAMP(g, 0, max);
			op->data.rgb.b[i][j] = CLAMP(b, 0, max);
		}
	}
}