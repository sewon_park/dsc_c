#pragma once
#include <math.h>

#include "dsc_utils.h"
#include "dsc_types.h"
#include "vdo.h"
#include "functions.h"

#define ORI  0
#define MODI 1
#define UPDATE 1
#define REAL   0
class ICH : Functions
{
private:
	typedef struct dsc_history_s {
		unsigned int *pixels[NUM_COMPONENTS];  //< Array of pixels that corresponds to the shift register (position 0 is MRU)
		int *valid;				  //< Valid bits for each pixel in the history (position 0 is MRU)
    #if MODI
        int vector[ICH_SIZE];    //jump
        int hit_loc[PIXELS_PER_GROUP];
        int hit_miss[PIXELS_PER_GROUP];
        unsigned int padding[NUM_COMPONENTS][ICH_SIZE+3];
        unsigned int valid_padding[ICH_SIZE];
        int onlyone;
    #endif
	} dsc_history_t;

    dsc_history_t history;	//< The ICH
	unsigned int ichPixels[PIXELS_PER_GROUP][NUM_COMPONENTS];  //< ICH pixel samples selected for current group (for encoder)

public:
	const int QuantDivisor[13] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };
	int origWithinQerr[PIXELS_PER_GROUP];   //< Encoder flags indicating that original pixels are within the quantization error
	int maxIchError[NUM_COMPONENTS];  //< Max error for each component using ICH

	//warpping function
	void Init(dsc_cfg_t *dsc_cfg);

	//default function
	void UpdateICHistory_DEC(int slice_width, int pic_width, int **prevLine, int first_line_flag, int group_num, int prevIchSelected, int currLine[NUM_COMPONENTS][BUFFER_FIX],FILE *log);
	void HistoryLookup(int slice_width, int group_num,int **prevLine, int entry, unsigned int *p, int first_line_flag);
	void UpdateHistoryElement(int slice_width, int first_line_flag, int group_num, int *valid, int prevIchSelected, unsigned int **pixels, int **prevLine, unsigned int *recon);
    #if MODI
    void UpdateICH_test_DEC (int slice_width, int pic_width, int **prevLine, int first_line_flag, int group_num, int prevIchSelected, int currLine[NUM_COMPONENTS][BUFFER_FIX],FILE *log);
    void UpdateHistory_test  (int slice_width, int first_line_flag, int group_num, int onlyone,int *valid, int prevIchSelected, unsigned int **pixels, int **prevLine, unsigned int recon[NUM_COMPONENTS][PIXELS_PER_GROUP],int mode);
    void TakeHistory (int location, int vector, int n,int onlyone,unsigned int **pixels,unsigned int padding[NUM_COMPONENTS][ICH_SIZE+3],int prevIchSelected); //location of current, vector, #of hit
    void TakeHistory_rv1 (int location,int vector,unsigned int **pixels,unsigned int padding[NUM_COMPONENTS][ICH_SIZE+3]); //location of current, vector, #of hit
    //int  HitMissDetect(unsigned int p[NUM_COMPONENTS][PIXELS_PER_GROUP],int id,int first_line_flag);
    #endif
};

