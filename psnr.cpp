#ifdef WIN32
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "vdo.h"
#include "psnr.h"
#else
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "vdo.h"
#include "psnr.h"
#endif



/*
	For RGB inputs we will compute the PSNR over all three channels.
	For YCbCr inputs we will only compute the PSNR over the luma channel.
*/
void compute_and_display_PSNR(pic_t *p_in, pic_t *p_out, int bpp, FILE *logfp)
{
	int xcnt, ycnt;
	int Max, ch, wd = 0;
	double sumSqrError = 0.0f;
	double psnr = 0.0f;
	double mse = 0.0f;
	int err = 0;
	int maxErrR = 0;
	int maxErrG = 0;
	int maxErrB = 0;

    
	switch(bpp) {
	case 8:
		Max = 255;
		break;
	case 10:
		Max = 1023;
		break;
	case 12:
		Max = 4095;
		break;
	case 16:
		Max = 65535;
		break;
	default: 
		Max = 0;
		printf("====> ERROR: Unrecognized bpp = %d.\n", bpp);    
		break;    
	}

	if (p_in->bits != p_out->bits)
		printf("in out bits not matched\n");

	if (p_in->color == RGB) 
	{
		wd = p_in->w; 
		for(ycnt=0; ycnt<p_in->h; ycnt++) 
		{
			for(xcnt=0; xcnt<p_in->w; xcnt++) 
			{
				for (ch=0; ch<3; ch++) 
				{
					switch(ch) {
					case 0:
						err = p_out->data.rgb.r[ycnt][xcnt] - p_in->data.rgb.r[ycnt][xcnt];
						if (abs(err) > maxErrR) {
							maxErrR = abs(err);							
						}			
#ifdef GENERATE_ERROR_IMAGE
						p_out->data.rgb.r[ycnt][xcnt] = 512 + err;
#endif										
						break;
					case 1:
						err = p_out->data.rgb.g[ycnt][xcnt] - p_in->data.rgb.g[ycnt][xcnt];
						if (abs(err) > maxErrG) {
							maxErrG = abs(err);							
						}							
#ifdef GENERATE_ERROR_IMAGE
						p_out->data.rgb.g[ycnt][xcnt] = 512 + err;
#endif										
						break;
					case 2:
						err = p_out->data.rgb.b[ycnt][xcnt] - p_in->data.rgb.b[ycnt][xcnt];
						if (abs(err) > maxErrB) {
							maxErrB = abs(err);							
						}							
#ifdef GENERATE_ERROR_IMAGE
						p_out->data.rgb.b[ycnt][xcnt] = 512 + err;
#endif										
						break;
					}
					sumSqrError += (double) (err * err);
				}				
			}
		}
		if (sumSqrError != 0) {
			mse = sumSqrError / ((double) (p_in->h * p_in->w * 3));
			psnr = 10.0 * log10( (double) (Max*Max) / mse );
			fprintf(logfp, "PSNR over RGB channels = %6.2f  ", psnr);
		} else {
			fprintf(logfp, "PSNR over RGB channels = Inf   ");
		}
		fprintf(logfp, "Max{|error|} = %4d   (R =%4d, G =%4d, B =%4d)   \n", MAX(maxErrR, MAX(maxErrG, maxErrB)), maxErrR, maxErrG, maxErrB);

	} else {

		for(ycnt=0; ycnt<p_in->h; ycnt++) 
		{
			for(xcnt=0; xcnt<p_in->w; xcnt++) 
			{
				err = p_out->data.yuv.y[ycnt][xcnt] - p_in->data.yuv.y[ycnt][xcnt];
				sumSqrError += (double) (err * err);
			}
		}
		if (sumSqrError != 0) {
			mse = sumSqrError / ((double) (p_in->h * p_in->w));  // no factor of 3, only looking at luma channel
			psnr = 10.0 * log10( (double) (Max*Max) / mse );
			fprintf(logfp, "PSNR over luma (Y) channel = %6.2f  \n", psnr);
		} 
		else 
			fprintf(logfp, "PSNR over luma (Y) channel = Inf   \n");

		if (p_in->chroma == YUV_422)
			wd = p_in->w/2;
		else if (p_in->chroma == YUV_444)
			wd = p_in->w;
		else
			printf(" YUV 420 not supported\n");

		sumSqrError = 0;
		for(ycnt=0; ycnt<p_in->h; ycnt++) 
		{
			for(xcnt=0; xcnt<wd; xcnt++) 
			{
				err = p_out->data.yuv.u[ycnt][xcnt] - p_in->data.yuv.u[ycnt][xcnt];
				sumSqrError += (double) (err * err);
			}
		}
		if (sumSqrError != 0) {
			mse = sumSqrError / ((double) (p_in->h * wd));  // no factor of 3, only looking at luma channel
			psnr = 10.0 * log10( (double) (Max*Max) / mse );
			fprintf(logfp,"PSNR over chroma (U) channel = %6.2f  \n", psnr);
		} 
		else 
			fprintf(logfp,"PSNR over chroma (U) channel = Inf   \n");
	
		sumSqrError = 0;
		for(ycnt=0; ycnt<p_in->h; ycnt++) 
		{
			for(xcnt=0; xcnt<wd; xcnt++) 
			{
				err = p_out->data.yuv.v[ycnt][xcnt] - p_in->data.yuv.v[ycnt][xcnt];
				sumSqrError += (double) (err * err);
			}
		}
		if (sumSqrError != 0) {
			mse = sumSqrError / ((double) (p_in->h * wd));  // no factor of 3, only looking at luma channel
			psnr = 10.0 * log10( (double) (Max*Max) / mse );
			fprintf(logfp,"PSNR over chroma (V) channel = %6.2f  \n", psnr);
		} 
		else 
			fprintf(logfp,"PSNR over chroma (V) channel = Inf   \n");

	}		
}

