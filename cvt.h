#pragma once
#include<stdio.h>
#include"utl.h"
#include"logging.h"
#include"dsc_types.h"

class CVT
{
public:
	void Init();

	void rgb2ycocg(pic_t *ip, pic_t *op, int xstart, int ystart, int slice_height, int slice_width);
	void ycocg2rgb(pic_t *ip, pic_t *op, int xstart, int ystart, int slice_height, int slice_width);

};
