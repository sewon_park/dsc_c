#if !defined(LOGGING_H)
#define LOGGING_H 1

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

extern int global_verbose_level;

#if !defined(__GNUC__)
#define __attribute__(...) /* erase */
#endif

void Err(char* format, ...) __attribute__((noreturn, format(printf,1,2)));  /* error, with trace */
void UErr(char* format, ...) __attribute__((noreturn, format(printf,1,2))); /* user-feedback message, no trace */
void CErr(char* format, ...) __attribute__((noreturn, format(printf,1,2))); /* programming error/assert, with trace */
void PErr(char* format, ...) __attribute__((noreturn, format(printf,1,2))); /* system perror(3), with trace */

#define Assert(condition) Assert_func(condition, #condition, __LINE__, __FILE__, __FUNCTION__)
void Assert_func(int condition, const char* const condition_str,
                 int cline, const char* const cfile, const char* const cfunction);

#endif

