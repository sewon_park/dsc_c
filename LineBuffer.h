#pragma once
#include "dsc_types.h"
#include "vdo.h"
#include "functions.h"

class LineBuffer : Functions
{
private:
	int lbufWidth;

    int group_num_line_delay1;
    int group_num_line_delay2;
    int group_num_line_delay3;

public:
	int *line[NUM_COMPONENTS];

	void Init(dsc_cfg_t *dsc_cfg, pic_t *pic);
	void Init(dsc_cfg_t *dsc_cfg, int size);

	void UpdateByGroup(dsc_cfg_t * dsc_cfg, int group_num, int group_num_line, int** source_line, int* cpntBitDepth);

	void WirteCBuff(int current_group[PIXELS_PER_GROUP][NUM_COMPONENTS]);
	void UpdateCBuff(int cpntBitDepth[NUM_COMPONENTS]);
	void ReadCBuff(int current_buff[NUM_COMPONENTS][BUFFER_FIX]);

	void WritePBuff();
	void UpdatePBuff();
	void ReadPBuff();

};

