#pragma once
#include "dsc_types.h"
#include "vdo.h"
#include "multiplex.h"


class Functions
{
private:
	// The following are constants that are used in the code:
	int qlevel_luma_8bpc[16] = { 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 7 };
	int qlevel_chroma_8bpc[16] = { 0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 8, 8 };
	int qlevel_luma_10bpc[20] = { 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 7, 8, 9 };
	int qlevel_chroma_10bpc[20] = { 0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 10, 10, 10 };
	int qlevel_luma_12bpc[24] = { 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 9, 10, 11 };
	int qlevel_chroma_12bpc[24] = { 0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 12, 12, 12 };

	int *quant_table_luma;
	int *quant_tabel_chroma;
	
public:
	void InitQuantTable(dsc_cfg_t *dsc_cfg);

	int MapQpToQlevel(int qp, int cpnt);
	int MaxResidualSize(int *cpnt_bit_depth, int cpnt, int qp);
	int GetQpAdjPredSize(int *cpnt_bit_depth, int master_qp, int prev_master_qp, int cpnt, int* predicted_size);
	int IsFlatnessInfoSent(dsc_cfg_t *dsc_cfg, int qp);
	int FindResidualSize(int eq);
	int EscapeCodeSize(int y_bit_depth, int qp);
	int PredictSize(int *req_size);
	int SampToLineBuf(int* cpntBitDepth, int linebuf_depth, int x, int cpnt);

};
