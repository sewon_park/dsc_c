#include "Predictor.h"

void Predictor::Init(dsc_cfg_t *dsc_cfg)
{
	InitQuantTable(dsc_cfg);
	master_qp_enc = 0;
	for (int i = 0; i < MAX_UNITS_PER_GROUP; i++) {
		for (int j = 0; j < SAMPLES_PER_UNIT; j++) {
			midpoint_recon[i][j] = 0;
			quantized_residual_mid[i][j] = 0;
		}
	}
	prevLinePred = new PRED_TYPE[(dsc_cfg->slice_width + PRED_BLK_SIZE - 1) / PRED_BLK_SIZE];
	for (int i = 0; i < (dsc_cfg->slice_width + PRED_BLK_SIZE - 1) / PRED_BLK_SIZE; i++) {
		prevLinePred[i] = (PRED_TYPE)0;
	}
	for (int i = 0; i<NUM_COMPONENTS; ++i) {
		for (int j = 0; j<BP_RANGE; ++j) {
			for (int k = 0; k<BP_SIZE; ++k)
				lastErr[i][k][j] = 0;
		}
	}
	for (int i = 0; i<NUM_COMPONENTS; i++) {
		range[i] = 1 << dsc_cfg->bits_per_component;
	}
	if (dsc_cfg->convert_rgb) {
		range[1] *= 2;
		range[2] *= 2;
	}
}

void Predictor::Prediction(int firsit_line_flag, int group_num, int group_size, int qp, int *cpntBitDepth, int **currLine, int **prevLine, int* use_midpoint, int quantized_residual[MAX_UNITS_PER_GROUP][SAMPLES_PER_UNIT], int recon_x[PIXELS_PER_GROUP][NUM_COMPONENTS],FILE* log)
{
	int qlevel;
	PRED_TYPE pred2use;
	int pred_x = 0;
	int err_q = 0;
	int maxval = 0;

//#ifndef REVERSE_pixel_pred 
	for (int pixel = 0; pixel < group_size; ++pixel) {
//#else
//	for (int pixel = group_size - 1; pixel >= 0; --pixel) {
//#endif // !REVERSE_pixel_pred
#ifndef REVERSE_cpnt_pred
		for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
#else
		for (int cpnt = NUM_COMPONENTS - 1; cpnt >= 0; cpnt--) {
#endif // !REVERSE_cpnt_pred	

			qlevel = MapQpToQlevel(qp, cpnt);
			if (firsit_line_flag) {
				// Use left predictor.  Modified MAP doesn't make sense since there is no previous line.
				pred2use = PT_LEFT;
			}
			else {
				pred2use = prevLinePred[group_num];
			}
			pred_x = SamplePredict_DEC(quantized_residual,cpntBitDepth, prevLine[cpnt], currLine[cpnt], group_num, pixel, pred2use, qlevel, cpnt);
			fprintf(log, "%3d ", pred_x);
			// Decoder takes error from bitstream
			err_q = quantized_residual[cpnt][pixel];

			// Use midpoint prediction if selected
			if (use_midpoint[cpnt])
				pred_x = FindMidpoint(cpntBitDepth, left_recon, cpnt, qlevel);

			// reconstruct
			// *MODEL NOTE* MN_IQ_RECON
			maxval = range[cpnt] - 1;
			recon_x[pixel][cpnt] = CLAMP(pred_x + (err_q << qlevel), 0, maxval);
		}
	}
	fprintf(log, "\n");
}

void Predictor::SelectValue(int group_num, int group_size, int ichSelected, unsigned int ich[3][3], int pred[3][3], int sel[3][3])
{
	// Save reconstructed value in line store
#ifndef REVERSE_pixel_sel
	for (int pixel = 0; pixel < group_size; ++pixel) {
#else
	for (int pixel = group_size - 1; pixel >= 0; --pixel) {
#endif // !REVERSE_pixel_sel	
#ifndef REVERSE_cpnt_sel
		for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
#else
		for (int cpnt = NUM_COMPONENTS - 1; cpnt >= 0; cpnt--) {
#endif // !REVERSE_cpnt_sel	
			if (ichSelected) {
				sel[pixel][cpnt] = ich[pixel][cpnt];
			}
			else {
				sel[pixel][cpnt] = pred[pixel][cpnt];
			}
		}
	}

	// Pad partial group at the end of the line
	if (group_size < PIXELS_PER_GROUP) {
		for (int i = group_size; i<PIXELS_PER_GROUP; ++i) {
			for (int cpnt = 0; cpnt<NUM_COMPONENTS; cpnt++) {
				sel[i][cpnt] = sel[group_size - 1][cpnt];
			}
		}
	}
}

void Predictor::UpdateLeftRecon_DEC(dsc_cfg_t * dsc_cfg, int leftest_pixel[NUM_COMPONENTS])
{
	for (int cpnt = 0; cpnt < NUM_COMPONENTS; ++cpnt)
		left_recon[cpnt] = leftest_pixel[cpnt];
}


int Predictor::SamplePredict_DEC(int quantized_residual[MAX_UNITS_PER_GROUP][SAMPLES_PER_UNIT], int* cpntBitDepth, int* prevLine, int currLine[BUFFER_FIX], int group_num, int pixel, PRED_TYPE predType, int qLevel, int cpnt)
{
	int a, b, c, d, e, f, g;
	int filt_b, filt_c, filt_d, filt_e;
	int blend_b, blend_c, blend_d, blend_e;
	int p;
	int bp_offset;
	int diff = 0;
	int h_offset_array_idx;

	h_offset_array_idx = group_num * 3 + PADDING_LEFT;

	// organize samples into variable array defined in dsc spec
	g = prevLine[h_offset_array_idx - 2];
	c = prevLine[h_offset_array_idx - 1];
	b = prevLine[h_offset_array_idx];
	d = prevLine[h_offset_array_idx + 1];
	e = prevLine[h_offset_array_idx + 2];
	f = prevLine[h_offset_array_idx + 3];
	a = (group_num == 0) ? 1 << (cpntBitDepth[cpnt] - 1) : currLine[BUFFER_FIX - PIXELS_PER_GROUP - 1];//do this in prev line buffer module

	switch (predType) {
	case PT_MAP:	// MAP prediction
					// *MODEL NOTE* MN_MMAP
//#define FILT3(a,b,c) (((a)+2*(b)+(c)+2)>>2)
#define FILT3(a,b,c) ((a+ (b<<1) + c + 2)>>2)
		filt_c = FILT3(g, c, b);
		filt_b = FILT3(c, b, d);
		filt_d = FILT3(b, d, e);
		filt_e = FILT3(d, e, f);
		diff = CLAMP(filt_c - c, -(QuantDivisor[qLevel] / 2), QuantDivisor[qLevel] / 2);
		blend_c = c + diff;
		diff = CLAMP(filt_b - b, -(QuantDivisor[qLevel] / 2), QuantDivisor[qLevel] / 2);
		blend_b = b + diff;
		diff = CLAMP(filt_d - d, -(QuantDivisor[qLevel] / 2), QuantDivisor[qLevel] / 2);
		blend_d = d + diff;
		diff = CLAMP(filt_e - e, -(QuantDivisor[qLevel] / 2), QuantDivisor[qLevel] / 2);
		blend_e = e + diff;

		// Pixel on line above off the raster to the left gets same value as pixel below (ie., midpoint)
		if (group_num == 0)
			blend_c = a;

		if (pixel == 0)  // First pixel of group
			p = CLAMP(a + blend_b - blend_c, MIN(a, blend_b), MAX(a, blend_b));
		else if (pixel == 1)   // Second pixel of group
			p = CLAMP(a + blend_d - blend_c + (quantized_residual[cpnt][0] * QuantDivisor[qLevel]),
				MIN(MIN(a, blend_b), blend_d), MAX(MAX(a, blend_b), blend_d));
		else    // Third pixel of group
			p = CLAMP(a + blend_e - blend_c + (quantized_residual[cpnt][0] + quantized_residual[cpnt][1])*QuantDivisor[qLevel],
				MIN(MIN(a, blend_b), MIN(blend_d, blend_e)), MAX(MAX(a, blend_b), MAX(blend_d, blend_e)));
		break;
	case PT_LEFT:
		p = a;    // First pixel of group
		if (pixel == 1)   // Second pixel of group
			p = CLAMP(a + (quantized_residual[cpnt][0] * QuantDivisor[qLevel]), 0, (1 << cpntBitDepth[cpnt]) - 1);
		else if (pixel == 2)  // Third pixel of group
			p = CLAMP(a + (quantized_residual[cpnt][0] + quantized_residual[cpnt][1])*QuantDivisor[qLevel],
				0, (1 << cpntBitDepth[cpnt]) - 1);
		break;
	default:  // PT_BLOCK+ofs = BLOCK predictor, starts at -1
			  // *MODEL NOTE* MN_BLOCK_PRED
		bp_offset = (int)predType - (int)PT_BLOCK;
		p = currLine[BUFFER_FIX - PIXELS_PER_GROUP + pixel - 1 - bp_offset];
		break;
	}

	return p;
}

void Predictor::BlockPredSearch_DEC(int* cpntBitDepth, int linebuf_depth, int block_pred_enable, int currLine[NUM_COMPONENTS][BUFFER_FIX], int group_num, int group_size,FILE* log_err, FILE* log_edge, FILE* log_bpcount, FILE* log_bpsad)
{
	//int candidate_vector;  // a value of 0 maps to -1, 2 maps to -3, etc.
	int pred_x;
	int min_err;
	PRED_TYPE min_pred;
	int pixdiff;
	int bp_sads[BP_RANGE];
	int modified_abs_diff;
	int recon_x;

	// This function runs right after a reconstructed value is determined and computes the best predictor to use for the NEXT line.
	// An implementation could run the block prediction search at any time after that up until the point at which the selection is needed.
	// *MODEL NOTE* MN_BP_SEARCH

	if (group_num == 0) {
		// Reset prediction accumulators every line
		bpCount = 0;
		lastEdgeCount = 10;  // Arbitrary large value as initial condition
		for (int cpnt_ = 0; cpnt_<NUM_COMPONENTS; ++cpnt_) {
			for (int group_ = 0; group_<BP_SIZE; ++group_) {
				for (int candidate_vector = 0; candidate_vector<BP_RANGE; ++candidate_vector)
					lastErr[cpnt_][group_][candidate_vector] = 0;
			}
		}
		for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
			fprintf(log_err, "cpnt %3d :", cpnt);
			for (int candidate_vector = 0; candidate_vector < BP_RANGE; ++candidate_vector)
				fprintf(log_err, "%3d ", lastErr[cpnt][group_num % BP_SIZE][candidate_vector]);
		}
		fprintf(log_err, "\n");
		fprintf(log_edge, "%3d %3d %3d \n", lastEdgeCount, lastEdgeCount, lastEdgeCount);
		//fprintf(log_bpcount, "%3d\n", bpCount);
	}

	int pixel_count = 0, cpnt_count = 0;
#ifndef REVERSE_pixel_bps
	for (int pixel = 0; pixel < group_size; ++pixel) {
#else
	for (int pixel = group_size - 1; pixel >= 0; --pixel) {
#endif // !REVERSE_pixel_bps	
#ifndef REVERSE_cpnt_bps
		for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
#else
		for (int cpnt = NUM_COMPONENTS - 1; cpnt >= 0; cpnt--) {
#endif // !REVERSE_cpnt_bps	
			recon_x = currLine[cpnt][BUFFER_FIX - PIXELS_PER_GROUP + pixel];

			// Last edge count check - looks at absolute differences between adjacent pixels
			//   - Don't use block prediction if the content is basically flat
			if (cpntBitDepth[cpnt] - linebuf_depth <= 0) {
				pixdiff = recon_x - currLine[cpnt][BUFFER_FIX - PIXELS_PER_GROUP + pixel - 1];
			}
			else {
				pixdiff = SampToLineBuf(cpntBitDepth, linebuf_depth, recon_x, cpnt) -
					SampToLineBuf(cpntBitDepth, linebuf_depth, currLine[cpnt][BUFFER_FIX - PIXELS_PER_GROUP + pixel - 1], cpnt);
			}

			pixdiff = ABS(pixdiff);
			if (cpnt_count == 0)
				edgeDetected = 0;
			if (pixdiff > (BP_EDGE_STRENGTH << (cpntBitDepth[0] - 8)))
				edgeDetected = 1;
			if (cpnt_count == NUM_COMPONENTS - 1) {
				if (edgeDetected)
					lastEdgeCount = 0;
				else
					lastEdgeCount++;
			}

			// The BP

			for (int candidate_vector = 0; candidate_vector < BP_RANGE; candidate_vector++) {
				if (pixel_count == 0) {
					// pred_err is summed over PRED_BLK_SIZE pixels
					pred_err[cpnt][candidate_vector] = 0;
				}

				pred_x = (group_num*PIXELS_PER_GROUP + pixel - 1 - candidate_vector) < 0 ? 1 << (cpntBitDepth[cpnt] - 1) : currLine[cpnt][BUFFER_FIX - PIXELS_PER_GROUP + pixel - 1 - candidate_vector];

				// HW uses previous line's reconstructed samples, which may be bit-reduced
				if (cpntBitDepth[cpnt] - linebuf_depth > 0) {
					pred_x = SampToLineBuf(cpntBitDepth, linebuf_depth, pred_x, cpnt);
					recon_x = SampToLineBuf(cpntBitDepth, linebuf_depth, recon_x, cpnt);
				}

				pixdiff = recon_x - pred_x;
				pixdiff = ABS(pixdiff);
				modified_abs_diff = MIN(pixdiff >> (cpntBitDepth[cpnt] - 7), 0x3f);
				// ABS differences are clamped to 6 bits each, pred_err for 3 pixels is 8 bits
				pred_err[cpnt][candidate_vector] += modified_abs_diff;
			}

			if (pixel_count == PRED_BLK_SIZE - 1) {
				// Track last 3 3-pixel SADs for each component (each is 7 bit)
				for (int candidate_vector = 0; candidate_vector < BP_RANGE; ++candidate_vector)
					lastErr[cpnt][group_num % BP_SIZE][candidate_vector] = pred_err[cpnt][candidate_vector];

				if (cpnt_count < NUM_COMPONENTS - 1) {
					cpnt_count++;
					continue;   // SAD is across all 3 components -- wait until we've processed all 3

				}

				for (int candidate_vector = 0; candidate_vector < BP_RANGE; ++candidate_vector) {
					bp_sads[candidate_vector] = 0;

					for (int group_ = 0; group_ < BP_SIZE; ++group_) {
						int sad3x1 = 0;

						// Use all 3 components
						for (int cpnt_ = 0; cpnt_ < NUM_COMPONENTS; ++cpnt_)
							sad3x1 += lastErr[cpnt_][group_][candidate_vector];

						// sad3x1 is 9 bits
						sad3x1 = MIN(511, sad3x1);

						bp_sads[candidate_vector] += sad3x1;  // 11-bit SAD
					}
					// Each bp_sad can have a max value of 63*9 pixels * 3 components = 1701 or 11 bits
                    //fprintf(log_bpsad, "%3d ", bp_sads[candidate_vector]);//for bs_sad_x
                    bp_sads[candidate_vector] >>= 3;  // SAD is truncated to 8-bit for comparison
				}
                //fprintf(log_bpsad, "\n");//for bs_sad_x

				min_err = 1000000;
				min_pred = PT_MAP;
				for (int candidate_vector = 0; candidate_vector < BP_RANGE; ++candidate_vector) {
					// Can't use -2 vector
					if (candidate_vector == 1)
						continue;
					// Ties favor smallest vector
					if (min_err > bp_sads[candidate_vector]) {
						min_err = bp_sads[candidate_vector];
						min_pred = (PRED_TYPE)(candidate_vector + PT_BLOCK);
					}
                    fprintf(log_bpsad, "%3d ", bp_sads[candidate_vector]);//for bs_sad
                }
                fprintf(log_bpsad, "\n");//for bs_sad

				if (block_pred_enable && (group_num*PIXELS_PER_GROUP + pixel >= 9))  // Don't start algorithm until 10th pixel
				{
					if (min_pred > PT_BLOCK)
						bpCount++;
					else
						bpCount = 0;
				}
				if ((bpCount >= 3) && (lastEdgeCount < BP_EDGE_COUNT))
					prevLinePred[group_num] = (PRED_TYPE)min_pred;
				else
					prevLinePred[group_num] = (PRED_TYPE)PT_MAP;
			}
			cpnt_count++;
			
			
			//fprintf(log_bpcount, "%3d ", bpCount);
		}
		//fprintf(log_bpcount, "%3d ", bpCount);
		fprintf(log_edge, "%3d ", lastEdgeCount);
		cpnt_count = 0;
		pixel_count++;
	}
	for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
		fprintf(log_err, "cpnt %3d :", cpnt);
		for (int candidate_vector = 0; candidate_vector < BP_RANGE; ++candidate_vector)
			fprintf(log_err, "%3d ", lastErr[cpnt][group_num % BP_SIZE][candidate_vector]);
	}
	fprintf(log_bpcount, "%3d\n", bpCount);
	
		fprintf(log_edge, "\n");
		fprintf(log_err, "\n");
}

int Predictor::FindMidpoint(int* cpntBitDepth, int* leftRecon, int cpnt, int qlevel)
{
	int range;

	// *MODEL NOTE* MN_MIDPOINT_PRED
	range = 1 << cpntBitDepth[cpnt];

	return (range / 2 + (leftRecon[cpnt] % (1 << qlevel)));
}
