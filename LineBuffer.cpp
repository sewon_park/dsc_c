#include "LineBuffer.h"

void LineBuffer::Init(dsc_cfg_t *dsc_cfg, pic_t *pic)
{
	lbufWidth = dsc_cfg->slice_width + PADDING_LEFT + PADDING_RIGHT;
	for (int i = 0; i<NUM_COMPONENTS; i++) {
		int initValue;
		if (dsc_cfg->convert_rgb) {
			initValue = 1 << (pic->bits - 1);
			if (i != 0)
				initValue *= 2;
		}
		else
			initValue = 1 << (pic->bits - 1);

		line[i] = new int[lbufWidth];
		for (int j = 0; j<lbufWidth; j++) {
			line[i][j] = initValue;
		}
	}
}

void LineBuffer::Init(dsc_cfg_t *dsc_cfg, int size)
{
	for (int i = 0; i<NUM_COMPONENTS; i++) {
		int initValue;
		if (dsc_cfg->convert_rgb) {
			initValue = 1 << (dsc_cfg->bits_per_component - 1);
			if (i != 0)
				initValue *= 2;
		}
		else
			initValue = 1 << (dsc_cfg->bits_per_component - 1);

		line[i] = new int[size];
		for (int j = 0; j<size; j++) {
			line[i][j] = initValue;
		}
	}
}

void LineBuffer::UpdateByGroup(dsc_cfg_t * dsc_cfg, int group_num, int group_num_line, int** source_line, int* cpntBitDepth)
{
	// first pixel padding
	if (group_num_line_delay3 == 0) {
		for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
			line[cpnt][PADDING_LEFT - 1] = SampToLineBuf(cpntBitDepth, dsc_cfg->linebuf_depth, source_line[cpnt][BUFFER_FIX - 4 * PIXELS_PER_GROUP], cpnt);
		}
	}

    if (group_num_line_delay3 == (dsc_cfg->slice_width + 2)/3 - 1) {
        for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
            line[cpnt][dsc_cfg->slice_width + PADDING_LEFT] = SampToLineBuf(cpntBitDepth, dsc_cfg->linebuf_depth, source_line[cpnt][BUFFER_FIX - 4 * PIXELS_PER_GROUP + 2], cpnt);
            line[cpnt][dsc_cfg->slice_width + PADDING_LEFT + 1] = SampToLineBuf(cpntBitDepth, dsc_cfg->linebuf_depth, source_line[cpnt][BUFFER_FIX - 4 * PIXELS_PER_GROUP + 2], cpnt);
        }
    }

	// copy line after 2 group
	if (group_num > 2) {
		for (int pixel = 0; pixel < PIXELS_PER_GROUP; pixel++) {
			for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
				line[cpnt][PADDING_LEFT + (group_num_line_delay3)*PIXELS_PER_GROUP + pixel] = SampToLineBuf(cpntBitDepth, dsc_cfg->linebuf_depth, source_line[cpnt][BUFFER_FIX - 4 * PIXELS_PER_GROUP + pixel], cpnt);
			}
		}
	}
    group_num_line_delay3 = group_num_line_delay2;
    group_num_line_delay2 = group_num_line_delay1;
    group_num_line_delay1 = group_num_line;
}

void LineBuffer::WirteCBuff(int current_group[PIXELS_PER_GROUP][NUM_COMPONENTS])
{
	for (int pixel = 0; pixel < PIXELS_PER_GROUP; pixel++) {
		for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
			line[cpnt][BUFFER_FIX - PIXELS_PER_GROUP + pixel] = current_group[pixel][cpnt];
		}
	}
}

void LineBuffer::UpdateCBuff(int cpntBitDepth[NUM_COMPONENTS])
{
	// shifting and initialize
	for (int i = 0; i < BUFFER_FIX - PIXELS_PER_GROUP; i++) {
		for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
			line[cpnt][i] = line[cpnt][i + PIXELS_PER_GROUP];
		}
	}
}

void LineBuffer::ReadCBuff(int current_buff[NUM_COMPONENTS][BUFFER_FIX])
{
	for (int pixel = 0; pixel < BUFFER_FIX; pixel++) {
		for (int cpnt = 0; cpnt < NUM_COMPONENTS; cpnt++) {
			current_buff[cpnt][pixel] = line[cpnt][pixel];
		}
	}
}
