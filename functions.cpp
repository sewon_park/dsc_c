#include "functions.h"

void Functions::InitQuantTable(dsc_cfg_t * dsc_cfg)
{
	// Initialize quantization table
	if (dsc_cfg->bits_per_component == 12)
	{
		quant_table_luma = qlevel_luma_12bpc;
		quant_tabel_chroma = qlevel_chroma_12bpc;
	}
	else if (dsc_cfg->bits_per_component == 10) {
		quant_table_luma = qlevel_luma_10bpc;
		quant_tabel_chroma = qlevel_chroma_10bpc;
	}
	else {
		quant_table_luma = qlevel_luma_8bpc;
		quant_tabel_chroma = qlevel_chroma_8bpc;
	}
}

int Functions::MapQpToQlevel(int qp, int cpnt)
{
	int qlevel;

	// *MODEL NOTE* MN_MAP_QP_TO_QLEVEL
	if (cpnt == 0)
		qlevel = quant_table_luma[qp];
	else
		qlevel = quant_tabel_chroma[qp];

	return (qlevel);
}

int Functions::MaxResidualSize(int *cpnt_bit_depth, int cpnt, int qp)
{
	int qlevel;

	qlevel = MapQpToQlevel(qp, cpnt);

	return (cpnt_bit_depth[cpnt] - qlevel);
}

int Functions::GetQpAdjPredSize(int *cpnt_bit_depth, int master_qp, int prev_master_qp, int cpnt, int* predicted_size)
{
	int pred_size, max_size;
	int qlevel_old, qlevel_new;

	// *MODEL NOTE* MN_DSU_SIZE_PREDICTION
	pred_size = predicted_size[cpnt];

	qlevel_old = MapQpToQlevel( prev_master_qp, cpnt);
	qlevel_new = MapQpToQlevel( master_qp, cpnt);

	pred_size += qlevel_old - qlevel_new;
	max_size = MaxResidualSize(cpnt_bit_depth, cpnt, master_qp);
	pred_size = CLAMP(pred_size, 0, max_size - 1);

	return (pred_size);
}

int Functions::IsFlatnessInfoSent(dsc_cfg_t *dsc_cfg, int qp)
{
	return ((qp >= dsc_cfg->flatness_min_qp) && (qp <= dsc_cfg->flatness_max_qp));
}

int Functions::FindResidualSize(int eq)
{
	int size_e;

	// Find the size in bits of e
	if (eq == 0) size_e = 0;
	else if (eq >= -1 && eq <= 0) size_e = 1;
	else if (eq >= -2 && eq <= 1) size_e = 2;
	else if (eq >= -4 && eq <= 3) size_e = 3;
	else if (eq >= -8 && eq <= 7) size_e = 4;
	else if (eq >= -16 && eq <= 15) size_e = 5;
	else if (eq >= -32 && eq <= 31) size_e = 6;
	else if (eq >= -64 && eq <= 63) size_e = 7;
	else if (eq >= -128 && eq <= 127) size_e = 8;
	else if (eq >= -256 && eq <= 255) size_e = 9;
	else if (eq >= -512 && eq <= 511) size_e = 10;
	else if (eq >= -1024 && eq <= 1023) size_e = 11;
	else if (eq >= -2048 && eq <= 2047) size_e = 12;
	else if (eq >= -4096 && eq <= 4095) size_e = 13;
	else size_e = 14;

	return size_e;
}

int Functions::EscapeCodeSize(int y_bit_depth, int qp)
{
	int qlevel, alt_size_to_generate;

	qlevel = MapQpToQlevel(qp, 0);    // map to luma quant
	alt_size_to_generate = y_bit_depth + 1 - qlevel;

	return (alt_size_to_generate);
}

int Functions::PredictSize(int *req_size)
{
	int pred_size;

	pred_size = 2;  // +0.5 for rounding
	pred_size += req_size[0] + req_size[1];
	pred_size += 2 * req_size[2];
	pred_size >>= 2;

	return (pred_size);
}

int Functions::SampToLineBuf(int* cpntBitDepth, int linebuf_depth, int x, int cpnt)
{
	int shift_amount, round, storedSample;

	// *MODEL NOTE* MN_LINE_STORAGE
	shift_amount = MAX(cpntBitDepth[cpnt] - linebuf_depth, 0);
	if (shift_amount > 0)
		round = 1 << (shift_amount - 1);
	else
		round = 0;

	storedSample = MIN(((x + round) >> shift_amount), (1 << linebuf_depth) - 1);
	return (storedSample << shift_amount);
}